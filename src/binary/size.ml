(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2021 Functori - contact@functori.com                        *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

let int16 _ = 2
let int _ = 4
let int32 _ = 4
let int64 _ = 8
let char _ = 1
let bool _ = 1
let concat l = List.fold_left (+) 4 l
let chars l = List.length l
let option f x = 1 + match x with None -> 0 | Some x -> f x
let elem x = 4 + x

let unit _ = 0
let string s = elem (String.length s)
let bytes b = elem (Bytes.length b)
let hex (h : Crypto.H.t) = elem (String.length (h :> string) / 2)
let list f l = concat @@ List.map f l
let array f l = concat @@ Array.(to_list @@ map f l)
let any len _ = len

(* proto *)
let uzarith z =
  let bits = Z.numbits z in
  if bits = 0 then 1 else (bits + 6) / 7
let uint64 i = uzarith (Z.of_int64 i)
let zarith z = (Z.numbits z + 1 + 6) / 7
let pkh _ = 21
let contract _ = 22
let source_manager _ = 21
let pk s =
  if String.length s < 4 then assert false
  else match String.sub s 0 4 with
    | "edpk" -> 33
    | _ -> 34
let block_hash _ = 32
let signature _ = 64
let script_expr_hash _ = 32
let operations_hash _ = 32
let operation_hash _ = 32
let context_hash _ = 32
let nonce_hash _ = 32
let protocol_hash _ = 32
let chain_id _ = 4
let tx_rollup_hash _ = 20
let tx_inbox_hash _ = 32
let tx_message_result_hash _ = 32
let tx_commitment_hash _ = 32
let sc_rollup_hash _ = 20
let sc_commitment_hash _ = 32
let sc_state_hash _ = 32
let l2_address _ = 20
