open Tzfunc
open Proto

let hash b = b.hash
let predecessor b = b.header.shell.predecessor
let level b = Int32.to_int b.header.shell.level

let block_url s =
  let EzAPI.BASE node = Node.config.Node.node in
  Format.sprintf "%s/chains/main/blocks/%s" node s
let monitor_url () =
  let EzAPI.BASE node = Node.config.Node.node in
  Format.sprintf "%s/monitor/heads/main" node

let block id =
  let id = match id with `head -> "head" | `hash h -> h | `level l -> string_of_int l in
  Lwt.bind (Hstream.call (block_url id)) @@ function
  | Error e -> Lwt.return_error e
  | Ok s -> Lwt.return (EzEncoding.destruct_res full_block_enc.json s)

let listen f acc =
  Hstream.stream ~url:(monitor_url ()) (fun acc s ->
    let r = EzEncoding.(destruct_res (ignore_enc Json_encoding.(obj1 (req "hash" string))) s) in
    f acc r) acc

let util = { Crawl.hash; level; predecessor }
let chain = { Crawl.block; listen = `separate listen }
