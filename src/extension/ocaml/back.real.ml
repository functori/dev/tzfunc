open Ezjs_min_lwt
open Tzfunc
open Rp
open Ext
open Etypes

let popup = U.popup ~base:"notif.html" ~width:357 ~height:600

let info ~src ~id req =
  let> accounts = Storage.get_accounts () in
  match accounts with
  | [] ->
    let> () = popup ~id {req with req = Rq_permission {src;req=req.req}} in
    Lwt.return_ok None
  | accounts ->
    let> approved = match src with
      | `client -> Lwt.return true
      | `api {Jext_lwt.Types.url; _} ->
        let|> l = Storage.get_approved () in
        List.mem url l in
    if not approved then
      let> () = popup ~id {req with req = Rq_permission {src; req=req.req}} in
      Lwt.return_ok None
    else
      let> password = get_password () in
      match password with
      | None ->
        let> () = popup ~id {req with req = Rq_permission {src; req=req.req}} in
        Lwt.return_ok None
      | Some _password ->
        let|>? r = Storage.get_info ?name:req.account ?network:req.network accounts in
        Some r


include Jext_lwt.Background.Make(struct
    include Etypes

    let handle_config = None

    let handle_request ~src ~id req =
      let> r = info ~id ~src req in
      match r with
      | Ok None ->
        Lwt.return_ok None
      | Error e ->
        Lwt.return_error e
      | Ok (Some info) ->
        let base = info.i_network.n_url in
        match req.req with
        | Rq_rpc rpc ->
          let> r = Node.get_raw_rpc ~base rpc in
          begin match r with
            | Error e -> Lwt.return_error e
            | Ok s -> Lwt.return_ok (Some (Rs_rpc (_JSON##parse (string s))))
          end
        | Rq_sign rq ->
          let> () = popup ~id {req with req = Rq_permission {src; req=Rq_sign rq}} in
          Lwt.return_ok None
        | Rq_forge ser ->
          let operations = List.map (of_operation_request ~source:info.i_account.a_tz1) ser.ser_operations in
          let> r = Node.forge_manager_operations ?forge_method:ser.ser_forge_method ?remove_failed:ser.ser_remove_failed ~base
              ~get_pk:(fun () -> Lwt.return_ok info.i_account.a_edpk) operations in
          begin match r with
            | Error e -> Lwt.return_error e
            | Ok (bytes, protocol, branch, operations) ->
              let operations = List.map to_operation_request operations in
              Lwt.return_ok (Some (Rs_forge {bytes; protocol; branch; operations}))
          end
        | Rq_inject b ->
          let> r = Node.silent_inject ~base b in
          begin match r with
            | Error e -> Lwt.return_error e
            | Ok hash -> Lwt.return_ok (Some (Rs_inject hash))
          end
        | Rq_send rq ->
          let> () = popup ~id {req with req = Rq_permission {src; req=Rq_send rq}} in
          Lwt.return_ok None
        | Rq_info ->
          Lwt.return_ok (Some (Rs_info {pkh=info.i_account.a_tz1; pk=info.i_account.a_edpk; network=info.i_network}))
        | Rq_permission rq ->
          let> () = popup ~id {req with req = Rq_permission rq} in
          Lwt.return_ok None
        | Rq_callback_ok rs ->
          Lwt.return_ok (Some rs)
        | Rq_callback_error e ->
          Lwt.return_error e
  end)

let () = Hacl.ready (fun () -> ())
