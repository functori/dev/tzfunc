{%%template|
<v-state @logged="set_logged" @route="route" :path="path" class="text-center">
  <template #header="{file}">
    <v-header :info="info" @route="route" @logged="set_logged" :file="file"></v-header>
  </template>
  <template #default="{file}">
    <div :class="(file=='index') ? 'container' : ''">
      <div class="px-2 py-3" v-if="info">

        <v-operation v-if="path=='delegate'" :info="info" v-slot="{ set_forged }" :path="path">
          <v-delegation :info="info" @forged="set_forged" :path="path"></v-delegation>
        </v-operation>

        <v-view v-else-if="path=='view'"></v-view>

        <v-settings v-else-if="path=='settings'" :info="info" @logged="set_logged" :path="path" :file="file"></v-settings>

        <v-sign v-else-if="info.sign_request && info.request_id" :info="info" :req="info.sign_request" :id="info.request_id"></v-sign>

        <v-operation v-else-if="info.send_request && info.request_id!=undefined" :info="info" :req="info.send_request" :id="info.request_id" :path="path">
        </v-operation>

        <v-operation v-else :info="info" v-slot="{ set_forged }" :path="path">
          <v-transaction :info="info" @forged="set_forged" :path="path"></v-transaction>
        </v-operation>
      </div>
    </div>
  </template>
</v-state>
|}

open Ezjs_min

let%data file _app : string = fst @@ State.get_url_params ()
and path = ""
and locked = true
and info : State.logged option = None

let%meth [@noconv] set_logged app (l: State.logged_jsoo t optdef) =
  app##.info := l;
  app##.path := string ""
and [@noconv] route app (p: js_string t) =
  app##.path := p

let () = Hacl.ready (fun () -> ())

[%%app {conv; export; mount; components=[
  "v-json", Unsafe.global##._VueJsonPretty##.default;
  Sign; Create; Settings; State; Transaction; Delegation; Operation; Header;
  Micheline; View; Tz; Common.CopyEmpty; Common.CopyButton; Common.CopyContainer;
  Common.ClearInput];
}]
