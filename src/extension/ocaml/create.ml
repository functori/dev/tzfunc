{%%template|
<div>
  <div v-if="error">
    <v-copy :value="JSON.stringify(error)">
      <v-json :data="error"></v-json>
    </v-copy>
  </div>
  <div v-else>
    <div class="mb-2">
      <label class="form-label">Name</label>
      <input class="form-control form-control-sm" v-model="input.name" autofocus>
    </div>
    <div class="mb-2" v-if="input.mnemonic==''">
      <label class="form-label">Secret key</label>
      <input class="form-control form-control-sm" v-model="input.secret_key">
    </div>
    <div class="mb-2">
      <label class="form-label">
        <span>Mnemonic</span>
        <button class="btn btn-secondary btn-sm mx-2" @click="generate()">
          <i class="bi bi-arrow-counterclockwise"></i>
        </button>
      </label>
      <v-copy :value="input.mnemonic" variant="outline-secondary">
        <textarea class="form-control form-control-sm" v-model="input.mnemonic" rows="3"></textarea>
      </v-copy>
    </div>
    <slot></slot>
    <button :disabled="input.secret_key=='' && input.mnemonic=='' || processing" @click="create(password)" class="btn btn-secondary m-2">
      <span v-if="!processing">
        {{ input.secret_key=='' ? 'Create' : 'Import' }}
      </span>
      <div v-else class="spinner-border spinner-border-sm"></div>
    </button>
  </div>
</div>
|}

open Ezjs_min
open Ext
open Etypes
open Tzfunc
open Rp

type input = {
  ci_secret_key: string;
  ci_name: string;
  ci_mnemonic: string;
} [@@deriving jsoo {mut}]

let empty = { ci_secret_key = ""; ci_name = ""; ci_mnemonic = "" }

let%prop password : string option = None

let%data input : input = empty
and error : Rp.error option = None
and processing = false

let clear app =
  app##.input := input_to_jsoo empty;
  app##.error := undefined;
  app##.processing := _false

let emit_updated app = [%emit "updated" app]

let add_account ?edesk ~password ~sk app =
  let open Crypto in
  let edesk = match edesk with
    | Some edesk -> Some edesk
    | None ->
      match Box.encrypt ~count:4096 ~password sk with
      | None -> None
      | Some esk ->
        Some (Base58.encode ~prefix:Prefix.ed25519_encrypted_seed (Raw.mk esk)) in
  match edesk with
  | None ->
    app##.error := def @@ Rp_jsoo.error_to_jsoo @@ `generic ("create_error", "failed encryption");
    app##.processing := _false;
    Lwt.return_unit
  | Some a_edesk ->
    let pk = Sk.to_public_key (Sk.T.mk @@ Raw.mk sk) in
    let a_edpk = Pk.b58enc pk in
    let pkh = Pk.hash pk in
    let a_tz1 = Pkh.b58enc pkh in
    let a_name =
      let s = to_string app##.input##.name in
      if s = "" then a_tz1 else s in
    let|> _ = Storage.add_account { a_name; a_tz1; a_edpk; a_edesk } in
    clear app;
    emit_updated app

let%meth create app =
  let open Crypto in
  app##.processing := _true;
  EzLwtSys.run @@ fun () ->
  let> () = EzLwtSys.sleep 0.02 in
  let> password = match to_optdef to_string app##.password with
    | None ->
      let|> o = get_password () in
      Option.value ~default:"" o
    | Some p ->
      Lwt.return p in
  if password = "" then (
    app##.error := def @@ Rp_jsoo.error_to_jsoo @@ `generic ("create_error", "empty password");
    app##.processing := _false;
    Lwt.return_unit)
  else
    let ci = input_of_jsoo (app##.input :> input_jsoo t) in
    let r = match ci with
      | ci when ci.ci_mnemonic <> "" ->
        begin match Bip39.of_words (String.split_on_char ' ' ci.ci_mnemonic) with
          | None -> Error (`generic ("create_error", "wrong mnemonic"))
          | Some indices ->
            Ok (String.sub (Bip39.to_seed ~passphrase:password indices) 0 32, None)
        end
      | ci when ci.ci_secret_key <> "" ->
        if String.length ci.ci_secret_key >= 4 && String.sub ci.ci_secret_key 0 4 = "edsk" then
          match Sk.b58dec ci.ci_secret_key with
          | Ok sk -> Ok ((sk :> string), None)
          | _ -> Error (`generic ("import_error", "format of secret key not handled"))
        else if String.length ci.ci_secret_key >= 5 && String.sub ci.ci_secret_key 0 4 = "edesk" then
          try
            let esk = Base58.decode ~prefix:Prefix.ed25519_encrypted_seed ci.ci_secret_key in
            match Box.decrypt ~password (esk :> string) with
            | None -> Error (`generic ("import_error", "cannot decrypt edesk"))
            | Some sk -> Ok (sk, Some ci.ci_secret_key)
          with _ -> Error (`generic ("import_error", "cannot decode secret key"))
        else Error (`generic ("create_error", "wrong format for secret key ('edsk' or 'edesk')"))
      | _ -> Error (`generic ("create_error", "no mnemonic or secret key given")) in
    match r with
    | Error e ->
      app##.error := def @@ Rp_jsoo.error_to_jsoo e;
      app##.processing := _false;
      Lwt.return_unit
    | Ok (sk, edesk) -> add_account ?edesk ~password ~sk app

and generate app =
  let open Crypto in
  let x = String.init 20 (fun _ -> Char.chr @@ Random.int 256) in
  let y = Bip39.of_entropy x in
  let mnemonic = Bip39.to_words y in
  app##.input##.mnemonic := string (String.concat " " mnemonic)

[%%comp {name="v-create"; conv; modules=[Rp, Rp_jsoo]}]
