{%%template|
<div>
  <div class="mb-3">
    <label class="form-label">delegate</label>
    <v-clear size="sm" :value="delegate" @update="c=>{delegate=c; validate_delegate(c)}" autofocus :valid="valid.delegate" :offset="valid.delegate ? 3. : 0.">
    </v-clear>
  </div>

  <div>
    <div class="d-flex align-items-center">
      <div :class="(!visible) ? 'border flex-grow-1' : 'flex-grow-1'" style="height:0"></div>
      <button class="ml-2 mt-1 btn btn-link" data-bs-toggle="collapse" data-bs-target="#collapse-numbers" @click="visible=!visible">
        <i class="bi bi-caret-down-fill" v-if="!visible" scale="0.6"></i>
        <i class="bi bi-caret-up-fill" v-else scale="0.6"></i>
        Options
      </button>
    </div>
    <div id="collapse-numbers" class="collapse mt-2">
      <div class="mb-3">
        <label class="form-label">fee</label>
        <v-tz size="sm" @update="x=>fee=x" :opt="true"></v-tz>
      </div>
      <div class="mb-3">
        <label class="form-label">gas limit</label>
        <v-clear :value="gas_limit" @update="x=>gas_limit=x" size="sm" type="number"></v-clear>
      </div>
      <div class="mb-3">
        <label class="form-label">storage limit</label>
        <v-clear :value="storage_limit" @update="x=>storage_limit=x" size="sm" type="number"></v-clear>
      </div>
      <div class="mb-3">
        <label class="form-label">counter</label>
        <v-clear :value="counter" @update="x=>counter=x" size="sm" type="number"></v-clear>
      </div>
    </div>
  </div>

  <button @click="forge()" class="my-2 btn btn-secondary">Forge</button>
</div>
|}

open Ezjs_min
open Ext.Etypes
open Tzfunc.Rp
open Operation

type validity = {
  delegate: bool option; [@key "delegate"]
} [@@deriving jsoo {mut}]

let%prop info : State.logged = {req}

let%data delegate = ""
and fee : Tz.t option = None
and gas_limit = ""
and storage_limit = ""
and counter = ""
and visible = false
and valid = { delegate = None }

let%meth forge app =
  let account = account_of_jsoo app##.info##.account in
  let delegate = match to_string app##.delegate with
    | "" | "none" | "null" | "undelegate" -> None
    | s -> Some s in
  let fee = Option.map Tz.abs @@ to_optdef Tz.of_jsoo app##.fee in
  let gas_limit, storage_limit, counter =
    to_string app##.gas_limit_, to_string app##.storage_limit_, to_string app##.counter in
  let gas_limit = if gas_limit = "" then None else try Some (Z.of_string gas_limit) with _ -> None in
  let storage_limit = if storage_limit = "" then None else try Some (Z.of_string storage_limit) with _ -> None in
  let counter = if counter = "" then None else try Some (Z.of_string counter) with _ -> None in
  let ops = [ Tzfunc.Utils.delegation ~source:account.a_tz1 ?fee ?gas_limit ?storage_limit ?counter delegate ] in
  let|> r = Operation.forge_aux ~account ops in
  let f = match r with
    | Error e -> Ferror e
    | Ok res -> Finfo res in
  emit_forged app f

let%meth validate_delegate _app (dl: string) : bool option =
  let s = Common.address_validity dl in
  match s, dl with
  | Some false, ("none"|"null"|"undelegate") -> Some true
  | _ -> s

[%%comp {name="v-delegation"; conv}]
