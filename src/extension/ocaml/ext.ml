open Ezjs_min_lwt
open Tzfunc.Rp
module Etypes = Etypes
module Storage = Storage
open Etypes

module U = Jext_lwt.Utils.Make(Etypes)

let get_password () =
  let|> w = Chrome_lwt.Runtime.getBackgroundPage () in
  to_optdef to_string (Unsafe.coerce w)##.password

let set_password password =
  let|> w = Chrome_lwt.Runtime.getBackgroundPage () in
  let password_t = match password with
      | None -> undefined
      | Some pwd -> def (string pwd) in
  (Unsafe.coerce w)##.password := password_t

let of_operation_request ~source op =
  let man_numbers = {
    Proto.fee = Option.value ~default:(-1L) op.fee;
    counter = Option.value ~default:Z.zero op.counter;
    gas_limit = Option.value ~default:Z.minus_one op.gas_limit;
    storage_limit = Option.value ~default:Z.minus_one op.storage_limit } in
  let kind = match op.kind with
    | Rtransaction tr -> Proto.Transaction tr
    | Rdelegation d -> Proto.Delegation d
    | Rreveal r -> Proto.Reveal r
    | Rorigination o -> Proto.Origination o
    | Rconstant c -> Proto.Constant c
    | Rdeposits l -> Proto.Deposits_limit l
    | Rtransfer_ticket tt -> Proto.Transfer_ticket tt in
  let man_info = { Proto.source; kind } in
  { Proto.man_info; man_numbers; man_metadata = None }

let to_operation_request op =
  let open Proto in
  let kind = match op.man_info.kind with
    | Transaction tr -> Rtransaction tr
    | Delegation d -> Rdelegation d
    | Reveal d -> Rreveal d
    | Origination o -> Rorigination o
    | Constant c -> Rconstant c
    | Deposits_limit l -> Rdeposits l
    | Transfer_ticket tt -> Rtransfer_ticket tt
    | _ -> failwith "operation not handled" in
  { Etypes.fee = Some op.man_numbers.fee;
    counter = Some op.man_numbers.counter;
    gas_limit = Some op.man_numbers.gas_limit;
    storage_limit = Some op.man_numbers.storage_limit;
    source = Some op.man_info.source; kind }
