{%%template|
<div>
  <div v-if="forge_result.input!=undefined">
    <slot :set_forged="set_forged" :path="path"></slot>
  </div>
  <div v-else-if="forge_result.request && forge_result.request.editable && forge_result.request.operations.length == 1 && forge_result.request.operations[0].transaction!=undefined">
    <v-transaction :info="info" @forged="set_forged" :path="path" :request="forge_result.request"></v-transaction>
  </div>
  <div v-else-if="forge_result.request">
    <v-copy :value="JSON.stringify(forge_result.request.operations)">
      <v-json :data="forge_result.request.operations"></v-json>
    </v-copy>
    <button @click="forge" class="my-2 btn btn-secondary" :disable="processing">
      <span v-if="!processing">Forge</span>
      <div v-else class="spinner-border spinner-border-small"></div>
    </button>
  </div>
  <div v-else-if="forge_result.error">
    <v-copy :value="JSON.stringify(forge_result.error)">
      <v-json :data="forge_result.error" :deep="5" deepCollapseChildren></v-json>
    </v-copy>
    <button @click="cancel" class="my-2 btn btn-secondary">Cancel</button>
  </div>
  <div v-else-if="forge_result.info">
    <v-copy :value="JSON.stringify(forge_result.info.operations)">
      <v-json :data="forge_result.info.operations"></v-json>
    </v-copy>
    <pre>{{ forge_result.info.bytes }}</pre>
    <div v-if="inject_result==undefined" class="m-2">
      <button @click="inject()" :disabled="processing" class="btn btn-secondary">
        <span v-if="!processing">Inject</span>
        <div v-else class="spinner-border spinner-border-sm"></div>
      </button>
      <button @click="cancel" class="m-2 btn btn-secondary">Cancel</button>
    </div>
    <div v-else>
      <div v-if="inject_result.error">
        <v-copy :value="JSON.stringify(inject_result.error)">
          <v-json :data="inject_result.error" :deep="5" deepCollapseChildren></v-json>
        </v-copy>
        <button @click="cancel" class="my-2 btn btn-secondary">Cancel</button>
      </div>
      <div v-else-if="inject_result.hash" class="my-2">
        <div>
          <a :href="explorer_url + inject_result.hash" target="_blank">{{ inject_result.hash.substring(0, 12) }}</a>
          <v-copy-button variant="secondary" :value="inject_result.hash">
          </v-copy-button>
        </div>
        <button @click="cancel" class="my-2 btn btn-secondary">Back</button>
      </div>
    </div>
  </div>
</div>
|}

open Ezjs_min
open Ext.Etypes
open Tzfunc
open Rp

type forge_info = {
  fr_bytes: raw;
  fr_protocol: string;
  fr_branch: string;
  fr_proto_operations: Proto.script_expr Proto.manager_operation list;
  fr_operations: operation_request list
} [@@deriving jsoo {modules=[Proto, Proto_jsoo]}]

type forge =
  | Finput
  | Frequest of send_request [@mutable]
  | Finfo of forge_info [@mutable]
  | Ferror of Rp.error
[@@deriving jsoo {modules=[Rp, Rp_jsoo]}]

type inject =
  | Ierror of Rp.error
  | Ihash of string
[@@deriving jsoo {modules=[Rp, Rp_jsoo]}]

let%prop [@noconv] req : send_request_jsoo t optdef = undefined
and id : int option = None
and info : State.logged = {req}
and path = ""

let forge_remove_undefined r =
  let f a = a##.operations##forEach (wrap_callback (fun x _ _ -> remove_undefined x)) in
  match Optdef.to_option r##.request, Optdef.to_option r##.info with
   | None, None -> ()
   | Some a, _ -> f a
   | _, Some a -> f a

let forge_req r =
  match to_optdef send_request_of_jsoo r with
  | Some req -> Frequest req
  | _ -> Finput

let%data forge_result app : forge = forge_req app##.req
and inject_result : inject option = None
and processing = false

let forge_aux ~account ?remove_failed ?forge_method ops =
  let get_pk () = Lwt.return_ok account.a_edpk in
  let|>? fr_bytes, fr_protocol, fr_branch, fr_proto_operations =
    Node.forge_manager_operations ~get_pk ?remove_failed ?forge_method ops in
  let fr_operations = List.map Ext.to_operation_request fr_proto_operations in
  { fr_bytes; fr_protocol; fr_branch; fr_operations; fr_proto_operations }

let emit_forged app f =
  ignore [%emit "forged" app (forge_to_jsoo f)]

let send_req app r =
  let|> _ = match Optdef.to_option app##.id with
    | None -> Lwt.return_ok None
    | Some id -> Common.(send_req ~id @@ mk_request r) in
  app##.processing := _false

let inject_aux app =
  let> o = Ext.get_password () in match o with
  | None ->
    let e = `generic ("unlogged", "") in
    app##.inject_result_ := def @@ inject_to_jsoo @@ Ierror e;
    send_req app (Rq_callback_error e)
  | Some password ->
    match forge_of_jsoo app##.forge_result_ with
    | Finfo r ->
      let account = account_of_jsoo app##.info##.account in
      let> rs = Common.get_sign ~watermark:Crypto.Watermark.generic ~password account in
      begin match rs with
        | Error e ->
          app##.inject_result_ := def @@ inject_to_jsoo @@ Ierror e;
          app##.processing := _false;
          send_req app (Rq_callback_error e)
        | Ok sign ->
          let> r2 = Node.inject ~bytes:r.fr_bytes ~branch:r.fr_branch
              ~protocol:r.fr_protocol ~sign r.fr_proto_operations in
          match r2 with
          | Ok hash ->
            app##.inject_result_ := def @@ inject_to_jsoo @@ Ihash hash;
            let rs = Rs_send {
                hash; branch=r.fr_branch; protocol=r.fr_protocol;
                bytes=r.fr_bytes; operations=r.fr_operations } in
            app##.processing := _false;
            send_req app (Rq_callback_ok rs)
          | Error e ->
            app##.inject_result_ := def @@ inject_to_jsoo @@ Ierror e;
            app##.processing := _false;
            send_req app (Rq_callback_error e)
      end
    | _ ->
      app##.processing := _false;
      Lwt.return_unit

let%meth forge app =
  app##.processing := _true;
  EzLwtSys.run @@ fun () ->
  let> () = EzLwtSys.sleep 0.02 in
  match forge_of_jsoo app##.forge_result_ with
  | Frequest req ->
    let account = account_of_jsoo app##.info##.account in
    let ops = List.map (Ext.of_operation_request ~source:account.a_tz1) req.ser_operations in
    let|> r = forge_aux ~account ?remove_failed:req.ser_remove_failed ?forge_method:req.ser_forge_method ops in
    let f = match r with
      | Error e -> Ferror e
      | Ok i -> Finfo i in
    let r = forge_to_jsoo f in
    forge_remove_undefined r;
    app##.processing := _false;
    app##.forge_result_ := r
  | _ ->
    app##.processing := _false;
    Lwt.return_unit

and set_forged app f =
  forge_remove_undefined f;
  app##.forge_result_ := f

and inject app =
  app##.processing := _true;
  EzLwtSys.(run (fun () -> let> () = sleep 0.02 in inject_aux app))

and cancel app =
  app##.inject_result_ := undefined;
  app##.forge_result_ := forge_to_jsoo Finput

let%watch path app p _old =
  match p with
  | "reset" -> cancel app
  | _ -> ()

and [@noconv] req app r _old =
  let r = forge_req r in
  app##.forge_result_ := forge_to_jsoo r

let%comp explorer_url app =
  match to_optdef to_string app##.info##.network##.name with
  | Some "mainnet" -> def (string "https://tzkt.io/")
  | Some "ghostnet" -> def (string "https://ghostnet.tzkt.io/")
  | _ ->
    log "no explorer for network: %S" (to_string app##.info##.network##.url);
    undefined

[%%comp {name="v-operation"; conv}]
