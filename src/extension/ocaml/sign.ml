{%%template|
<div>
  <div v-if="unpacked">
    <v-copy :value="JSON.stringify(unpacked)">
      <v-json :data="unpacked"></v-json>
    </v-copy>
  </div>
  <pre>
    {{ ((req.watermark) ? req.watermark : '') + req.bytes }}
  </pre>
  <button v-if="result==undefined" @click="sign()" :disabled="processing" class="btn btn-secondary">
    <span v-if="!processing">Sign</span>
    <div v-else class="spinner-border spinner-border-sm"></div>
  </button>
  <div v-else-if="result.signature!=undefined">
    <v-copy :value="result.signature" variant="outline-secondary">
      <pre>{{ result.signature }}</pre>
    </v-copy>
  </div>
  <div v-else-if="result.error!=undefined">
    <v-copy :value="JSON.stringify(result.error)">
      <v-json :data="result.error"></v-json>
    </v-copy>
  </div>
</div>
|}

open Tzfunc
open Ezjs_min
open Rp
open Ext
open Etypes
open State

type result =
  | SRsignature of string
  | SRerror of Rp.error
[@@deriving jsoo {modules=[Rp, Rp_jsoo]}]

let%prop req : sign_request = {req}
and id : int = {req}
and info : logged = {req}

let%data result : result option = None
and processing = false

let send_req app r =
  let|> _ = Common.(send_req ~id:app##.id @@ mk_request r) in
  app##.processing := _false

let sign_aux app =
  let> o = get_password () in match o with
  | None ->
    let e = `generic ("unlogged", "") in
    app##.result := def @@ result_to_jsoo @@ SRerror e;
    send_req app (Rq_callback_error e)
  | Some password ->
    let req, info = sign_request_of_jsoo app##.req, logged_of_jsoo app##.info in
    let> r = Common.get_sign ?watermark:req.sir_watermark ~password info.l_account in
    match r with
    | Error e ->
      app##.result := def @@ result_to_jsoo @@ SRerror e;
      let|> _ = Common.send_req ~id:app##.id
          {Etypes.req=Rq_callback_error e; account=None; network=None} in
      app##.processing := _false
    | Ok f ->
      let> r = f req.sir_bytes in
      match r with
      | Error e ->
        app##.result := def @@ result_to_jsoo @@ SRerror e;
        send_req app (Rq_callback_error e)
      | Ok s ->
        let s = Crypto.Signature.(b58enc @@ T.mk s) in
        app##.result := def @@ result_to_jsoo @@ SRsignature s;
        send_req app (Rq_callback_ok (Rs_sign s))

let%meth sign app =
  app##.processing := _true;
  EzLwtSys.run (fun () -> let> () = EzLwtSys.sleep 0.02 in sign_aux app)

and unpacked app =
  let sir = sign_request_of_jsoo app##.req in
  match sir.sir_watermark with
  | None -> undefined
  | Some w ->
    try
      match Read.unpack ?typ:sir.sir_type @@ Crypto.concat_raw [w; sir.sir_bytes] with
      | Error _ -> undefined
      | Ok m -> def (Proto_jsoo.micheline_to_jsoo m)
    with _ -> undefined

[%%comp {name="v-sign"; conv}]
