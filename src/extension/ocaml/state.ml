{%%template|
<div>

  <slot name="header" :file="file"></slot>

  <!-- placeholder -->
  <div v-if="state.blank!=undefined" class="p-5"></div>

  <!-- create -->
  <div v-else-if="state.enable!=undefined" class="px-2 py-5">
    <v-create :password="password" @updated="set_state">
      <div class="mb-3">
        <label class="form-label">Choose a password *</label>
        <input type="password" v-model="password" class="form-control form-control-sm">
      </div>
    </v-create>
  </div>

  <!-- approve -->
  <div v-else-if="state.approve" class="px-2 py-5 my-5">
    <div class="m-2">{{ state.approve.name }}</div>
    <div class="m-2">
      <a :href="state.approve.url" target="_blank">{{ state.approve.url }}</a>
    </div>
    <div class="m-3">
      <img v-if="state.approve.icon" :src="state.approve.icon" :alt="state.approve.name" width="50">
    </div>
    <button @click="approve()" class="m-2 btn btn-secondary">Approve</button>
  </div>

  <!-- unlock -->
  <div v-else-if="state.unlock!=undefined" :class="'px-2 py-5 my-5 ' + ((file=='index') ? 'container' : '')">
    <div class="mb-3">
      <label class="form-label">Password</label>
      <input type="password" v-model="password" @keyup.enter="unlock()" class="form-control form-control-sm" autofocus>
    </div>
    <button v-if="!processing" @click="unlock()" class="m-2 btn btn-secondary">Unlock</button>
    <button v-else class="btn btn-secondary" disabled>
      <div class="spinner-border spinner-border-sm"></div>
    </button>
  </div>

  <!-- error -->
  <div v-else-if="state.error" :class="'px-2 py-5 ' + ((file=='index') ? 'container' : '')">
    <h5>Error {{ state.error.name }}</h5>
    <pre>{{ state.error.message }}</pre>
    <button @click="route('reset')" class="btn btn-secondary">Reset</button>
  </div>

  <div v-else-if="state.info!=undefined">
    <slot :file="file"></slot>
  </div>
</div>
|}

open Ezjs_min
open Ext
open Etypes
open Tzfunc
open Rp

type logged = {
  l_account: account; [@mutable]
  l_network: network; [@mutable]
  l_accounts: account list;
  l_node: Proto.account option;
  l_sign_request: sign_request option;
  l_send_request: send_request option;
  l_request_id: int option;
  l_request_account: bool;
  l_request_network: bool;
} [@@deriving jsoo {modules=[Proto, Proto_jsoo]}]

type state =
  | Blank [@key "blank"]
  | Enable [@key "enable"]
  | Approve of Jext_lwt.Types.site_metadata [@key "approve"]
  | Unlock [@key "unlock"]
  | Info [@key "info"]
  | State_error of {name:string; message:string} [@key "error"]
[@@deriving jsoo]

let get_url_params () =
  let url = to_string Dom_html.window##.location##.href in
  match String.rindex_opt url '/' with
  | None -> "", []
  | Some j ->
    let name = String.sub url (j+1) (String.length url - j - 1) in
    match String.rindex_opt name '?' with
    | None -> Filename.remove_extension name, []
    | Some i ->
      let file = String.sub name 0 i in
      let s = String.sub name (i+1) (String.length name - i - 1) in
      Filename.remove_extension file, List.filter_map (fun s -> match String.split_on_char '=' s with
        | [ k; v] -> Some (k, v)
        | _ -> None) @@ String.split_on_char '&' s

let%prop path = ""

let%data state : state = Blank
and password = ""
and file = ""
and id : int option = None
and req : request option = None
and processing = false

class type data = object
  method state: state_jsoo t prop
  method password: js_string t prop
  method file: js_string t readonly_prop
  method id: int optdef readonly_prop
  method req: request_jsoo t optdef readonly_prop
  method processing: bool t prop
end

let get_req_info () =
  let file, params = get_url_params () in
  let id = match List.assoc_opt "id" params with
    | None -> None
    | Some id -> try Some (int_of_string id) with _ -> None in
  let req = match List.assoc_opt "req" params with
    | None -> None
    | Some s ->
      try
        let r = Jext_lwt.Common.decode s in
        Some (request_of_jsoo r)
      with _ -> None in
  file, id, req

let get_logged ?id ?req ?accounts ~network ~account update =
  let _, id, req =
    if update then get_req_info ()
    else "", id, req in
  let> accounts = match accounts with
    | None -> Storage.get_accounts ()
    | Some accounts -> Lwt.return accounts in
  let EzAPI.BASE url = network.n_url in
  Tzfunc.Node.set_node url;
  let> _ = Tzfunc.Node.set_constants () in
  let> r = Tzfunc.Node.get_account_info account.a_tz1 in
  let l_node = Result.to_option r in
  let|> l_sign_request, l_send_request, l_request_account, l_request_network = match req with
    | Some ({ Etypes.req = Rq_permission { req; _ }; account; network } as r) ->
      let req_account = Option.is_some account in
      let req_network = Option.is_some network in
      begin match req with
        | Rq_sign sir -> Lwt.return (Some sir, None, req_account, req_network)
        | Rq_send ser -> Lwt.return (None, Some ser, req_account, req_network)
        | _ ->
          let|> _ = Common.send_req ?id {r with Etypes.req} in
          (None, None, false, false)
      end
    | Some _ -> Lwt.return (None, None, false, false)
    | _ -> Lwt.return (None, None, false, false) in
  { l_account=account; l_network=network; l_accounts=accounts;
    l_node; l_sign_request; l_send_request; l_request_id = id;
    l_request_account; l_request_network }

let emit_logged app l =
  ignore @@ [%emit "logged" app (optdef logged_to_jsoo l)]

let%meth [@noconv] set_state app : unit Lwt.t =
  app##.state := state_to_jsoo Blank;
  app##.password := string "";
  let> l_accounts = Storage.get_accounts () in
  match l_accounts with
  | [] ->
    app##.state := state_to_jsoo Enable;
    emit_logged app None;
    Lwt.return_unit
  | accounts ->
    let> password = get_password () in
    match password with
    | None ->
      app##.state := state_to_jsoo Unlock;
      emit_logged app None;
      Lwt.return_unit
    | Some _password ->
      let id = Optdef.to_option app##.id in
      let req = to_optdef request_of_jsoo app##.req in
      let> meta = match req with
        | Some {Etypes.req= Rq_permission {src = `api ({Jext_lwt.Types.url; _} as meta); _}; _} ->
          let> l = Storage.get_approved () in
          if List.mem url l then Lwt.return_none else Lwt.return_some meta
        | _ -> Lwt.return_none in
      match meta with
      | Some meta ->
        app##.state := state_to_jsoo @@ Approve meta;
        emit_logged app None;
        Lwt.return_unit
      | None ->
        let name, network = match req with
          | None -> None, None
          | Some req -> req.account, req.network in
        let> r = Storage.get_info ?name ?network accounts in
        match r with
        | Error ((`generic (name, message)) as e) ->
          app##.state := state_to_jsoo (State_error {name; message});
          begin match id with
            | None -> Lwt.return_unit
            | Some id ->
              let|> _ = Common.send_req ~id
                  {Etypes.req=Rq_callback_error e; account=None; network=None} in
              ()
          end
        | Ok info ->
          let|> logged = get_logged ?id ?req ~accounts ~account:info.i_account ~network:info.i_network false in
          emit_logged app (Some logged);
          app##.state := state_to_jsoo Info

and unlock app =
  app##.processing := _true;
  EzLwtSys.run @@ fun () ->
  let> () = EzLwtSys.sleep 0.02 in
  let password = to_string app##.password in
  let> accounts = Storage.get_accounts () in
  let> r = Common.get_sk ~password (List.hd accounts) in
  match r with
  | Error (`generic (name, message)) ->
    app##.state := state_to_jsoo (State_error {name; message});
    app##.processing := _false;
    Lwt.return_unit
  | Ok _ ->
    let> () = Ext.set_password (Some password) in
    app##.processing := _false;
    app##set_state_

and approve app =
  EzLwtSys.run @@ fun () ->
  match state_of_jsoo app##.state with
  | Approve meta ->
    let> () = Storage.set_approved meta in
    app##set_state_
  | _ -> app##set_state_

and [@noconv] route app (s: js_string t) =
  [%emit "route" app s]

let reset app =
  EzLwtSys.run @@ fun () -> app##set_state_

let lock app =
  EzLwtSys.run @@ fun () ->
  let> () = Ext.set_password None in
  app##set_state_

let%watch path app p _old = match p with
  | "reset" -> reset app
  | "lock" -> lock app
  | _ -> ()

let set_network ?accounts ~account app v =
  let network = match to_optdef to_string v with
    | Some v -> v
    | None ->
      match to_string app##.network_select_ with
      | "custom" -> to_string app##.network_input_
      | s -> s in
  let> network = Storage.set_network network in
  let network = Result.value ~default:{n_url= EzAPI.BASE "http://tz.functori.com"; n_name=Some "mainnet"} network in
  let|> logged = get_logged ?accounts ~account ~network true in
  emit_logged app (Some logged)

let onclose id =
  Dom_html.window##.onunload := Dom_html.handler @@ fun _ ->
    EzLwtSys.run (fun () ->
        let|> _ = Common.send_req ~id {
            req=Rq_callback_error (`generic ("closed_notif", "notification was aborted"));
            account = None; network = None } in
        ());
    _true

[%%created fun app ->
  let file, id, req = get_req_info () in
  begin match id, req with
    | Some id, Some _ -> onclose id
    | _ -> ()
  end;
  app##.file := string file;
  app##.id := Optdef.option id;
  app##.req := optdef request_to_jsoo req;
  Common.runa0 set_state app
]

[%%comp {name="v-state"; conv}]
