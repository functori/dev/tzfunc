open Ezjs_min
open Proto

let primitive_or_macro_to_jsoo = function
  | #primitive as p -> Proto_jsoo.primitive_to_jsoo p
  | `macro s -> string s

let primitive_of_macro_of_jsoo js =
  try (primitive_of_jsoo js :> primitive_or_macro)
  with _ ->  `macro (to_string js)
