(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2021 Functori - contact@functori.com                        *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Proto
open Binary.Writer

module Macros = Macros

let unmacro m = match m with
  | Mprim { prim = `macro s; args; annots } -> Macros.expand ~args ~annots s
  | _ -> Ok m

let forge_primitive (p : primitive_or_macro) = match p with
  | `macro s -> Error (`unknown_primitive (Some s))
  | #primitive as p ->
    match List.fold_left (fun (i, acc) (_, s) ->
        if acc = None && s = p then (i, Some i) else (i+1, acc)) (0, None) primitives with
    | _, None -> Error (`unknown_primitive None)
    | _, Some i -> uint8 i

let forge_annots l =
  elem @@ (Ok (mk @@ String.concat " " l))

let rec forge_micheline m =
  match unmacro m with
  | Error e -> Error e
  | Ok m -> match m with
    | Mint z -> concat [ uint8 0; zarith z ]
    | Mstring s -> concat [ uint8 1; string s ]
    | Mbytes h -> concat [ uint8 10; hex h ]
    | Mseq l ->
      concat [ uint8 2; elem (concat @@ List.map forge_micheline l) ]
    | Mprim {prim; args; annots} ->
      let prm = forge_primitive prim in
      match args, annots with
      | [], [] -> concat [ uint8 3; prm ]
      | [], an -> concat [ uint8 4; prm; forge_annots an ]
      | [ arg1 ], [] -> concat [ uint8 5; prm; forge_micheline arg1 ]
      | [ arg1 ], an ->
        concat [ uint8 6; prm; forge_micheline arg1; forge_annots an ]
      | [ arg1; arg2 ], [] ->
        concat [ uint8 7; prm; forge_micheline arg1; forge_micheline arg2 ]
      | [ arg1; arg2 ], an ->
        concat [ uint8 8; prm; forge_micheline arg1; forge_micheline arg2;
                 forge_annots an ]
      | args, an ->
        concat [ uint8 9; prm; elem @@ concat @@ List.map forge_micheline args;
                 forge_annots an ]

let forge_script_expr = function
  | Micheline sc -> elem (forge_micheline sc)
  | Bytes h -> hex h

let forge_entrypoint = function
  | EPdefault | EPnamed "default" -> uint8 0
  | EProot | EPnamed "root" -> uint8 1
  | EPdo | EPnamed "do" -> uint8 2
  | EPset | EPnamed "set" -> uint8 3
  | EPremove | EPnamed "remove" -> uint8 4
  | EPnamed s ->
    concat [ uint8 255; uint8 (String.length s); Ok (Crypto.Raw.mk s) ]

let forge_script {code; storage} =
  concat [ forge_script_expr code; forge_script_expr storage ]

let forge_transaction_options = function
  | None | Some {value=Micheline Mprim {prim=`Unit; args=[]; annots=[]}; entrypoint=EPdefault }
  | Some {value=Micheline Mprim {prim=`Unit; args=[]; annots=[]}; entrypoint=EPnamed "default" } ->
    let| s = bool false in
    Ok (108, s)
  | Some {value=Micheline sc_m; entrypoint} ->
    let| b = concat [ bool true; forge_entrypoint entrypoint; elem (forge_micheline sc_m) ] in
    Ok (108, b)
  | _ -> Error (`forge_error "cannot forge unknown script language")

let forge_manager_op ~src ~fee ~counter ~gas_limit ~storage_limit =
  concat [
    pkh src;
    uint64 fee;
    uzarith counter;
    uzarith gas_limit;
    uzarith storage_limit ]

let forge_reveal ~src ~numbers pubkey =
  let {fee; counter; gas_limit; storage_limit} = numbers in
  concat [
    uint8 107;
    forge_manager_op ~src ~fee ~counter ~gas_limit ~storage_limit;
    pk pubkey ]

let forge_transaction ~src ~numbers ~amount ~destination ~parameters =
  let {fee; counter; gas_limit; storage_limit} = numbers in
  let| tag, options = forge_transaction_options parameters in
  let dst = contract destination in
  concat [
    uint8 tag;
    forge_manager_op ~src ~fee ~counter ~gas_limit ~storage_limit;
    uint64 amount;
    dst ;
    Ok options ]

let forge_origination ~src ~numbers ~balance ~script =
  let {fee; counter; gas_limit; storage_limit} = numbers in
  concat [
    uint8 109;
    forge_manager_op ~src ~fee ~counter ~gas_limit ~storage_limit;
    uint64 balance;
    uint8 0 ;
    forge_script script
  ]

let forge_delegation ~src ~numbers delegate =
  let {fee; counter; gas_limit; storage_limit} = numbers in
  concat [
    uint8 110;
    forge_manager_op ~src ~fee ~counter ~gas_limit ~storage_limit;
    option pkh delegate
  ]

let forge_constant ~src ~numbers value =
  let {fee; counter; gas_limit; storage_limit} = numbers in
  concat [
    uint8 111;
    forge_manager_op ~src ~fee ~counter ~gas_limit ~storage_limit;
    forge_script_expr value
  ]

let forge_deposits ~src ~numbers limit =
  let {fee; counter; gas_limit; storage_limit} = numbers in
  concat [
    uint8 112;
    forge_manager_op ~src ~fee ~counter ~gas_limit ~storage_limit;
    option uint64 limit;
  ]

let forge_return_bond ~src ~numbers rollup =
  let {fee; counter; gas_limit; storage_limit} = numbers in
  concat [
    uint8 153;
    forge_manager_op ~src ~fee ~counter ~gas_limit ~storage_limit;
    tx_rollup_hash rollup;
  ]

let forge_transfer_ticket ~src ~numbers tt =
  let {fee; counter; gas_limit; storage_limit} = numbers in
  concat [
    uint8 158;
    forge_manager_op ~src ~fee ~counter ~gas_limit ~storage_limit;
    forge_script_expr tt.tt_contents;
    forge_script_expr tt.tt_type;
    contract tt.tt_ticketer;
    uint64 tt.tt_amount;
    contract tt.tt_destination;
    forge_entrypoint tt.tt_entrypoint;
  ]

let forge_manager_operation_kind ~src ~numbers = function
  | Reveal pk -> forge_reveal ~src ~numbers pk
  | Transaction {amount; destination; parameters} ->
    forge_transaction ~src ~numbers ~amount ~destination ~parameters
  | Origination {balance; script} -> forge_origination ~src ~numbers ~balance ~script
  | Delegation delegate -> forge_delegation ~src ~numbers delegate
  | Constant value -> forge_constant ~src ~numbers value
  | Deposits_limit limit -> forge_deposits ~src ~numbers limit
  | Transfer_ticket tt -> forge_transfer_ticket ~src ~numbers tt
  | _ -> Error `operation_not_handled

let forge_operation m =
  forge_manager_operation_kind ~src:m.man_info.source ~numbers:m.man_numbers
    m.man_info.kind

let forge_activation ~pkh ~secret = (* 41 bytes *)
  concat [
    uint8 4;
    Binary.Writer.pkh pkh;
    Ok (Crypto.hex_to_raw secret) ]

let forge_operations_base block ops_bytes =
  concat [ block_hash block; concat ops_bytes ]

let forge_operations block ops =
  forge_operations_base block @@ List.map forge_operation ops

let pack (p : micheline) (m : micheline) =
  let rec aux p m =
    match p, m with
    | Mprim { prim = `string; _}, _
    | Mprim { prim = `nat; _}, _
    | Mprim { prim = `int; _}, _
    | Mprim { prim = `bytes; _}, _
    | Mprim { prim = `bool; _}, _
    | Mprim { prim = `unit; _ }, _
    | Mprim { prim = `mutez; _ }, _-> forge_micheline m
    | Mprim { prim = `list; args = [ p ]; _ }, Mseq l
    | Mprim { prim = `set; args = [ p ]; _ }, Mseq l ->
      concat [ uint8 2; elem (concat @@ List.map (aux p) l) ]
    | Mprim { prim = `pair; args = [ p1; p2 ]; _ }, Mprim { prim=`Pair; args = [m1; m2]; _ }
    | Mprim { prim = `pair; args = [ p1; p2 ]; _ }, Mseq [m1; m2] ->
      concat [ uint8 7; forge_primitive `Pair; aux p1 m1; aux p2 m2 ]
    | Mprim { prim = `pair; args = [ p1; p2 ]; _ }, Mprim { prim=`Pair; args = (m1 :: m); _ }
    | Mprim { prim = `pair; args = [ p1; p2 ]; _ }, Mseq (m1 :: m) ->
      concat [ uint8 7; forge_primitive `Pair; aux p1 m1; aux p2 (Mseq m) ]
    | Mprim { prim = `pair; args = [ p1; p2 ]; _ }, Mprim { prim; args = [m1; m2]; _ } ->
      concat [ uint8 7; forge_primitive prim; aux p1 m1; aux p2 m2 ]
    | Mprim { prim = `pair; args = p1 :: p; _ }, Mprim { prim = `Pair; args = m1 :: m; _ }
    | Mprim { prim = `pair; args = p1 :: p; _ }, Mseq (m1 :: m) -> (* unsafe *)
      concat [ uint8 7; forge_primitive `Pair; aux p1 m1; aux (Mprim {prim=`pair; args=p; annots=[]}) (Mseq m) ]
    | Mprim { prim = `option; _ }, Mprim { prim = `None; _ } ->
      forge_micheline m
    | Mprim { prim = `option; args = [ p ]; _ }, Mprim { prim = `Some; args = [ m ]; _ } ->
      concat [ uint8 5; forge_primitive `Some; aux p m ]
    | Mprim { prim = `or_; args = [ p; _ ]; _ }, Mprim { prim = `Left; args = [ m ]; _ } ->
      concat [ uint8 5; forge_primitive `Left; aux p m ]
    | Mprim { prim = `or_; args = [ _; p ]; _ }, Mprim { prim = `Right; args = [ m ]; _ } ->
      concat [ uint8 5; forge_primitive `Right; aux p m ]
    | Mprim { prim = `map; args = [ pk; pv ]; _ }, Mseq l ->
      concat [ uint8 2; elem (concat @@ List.map (function
          | Mprim { prim = `Elt; args = [ k; v ]; _ } ->
            concat [ uint8 7; forge_primitive `Elt; aux pk k; aux pv v ]
          | _ -> Error `wrong_michelson_type) l) ]
    | Mprim { prim = `key; _ }, Mstring s ->
      let| s = pk s in
      forge_micheline (Mbytes (Crypto.hex_of_raw s))
    | Mprim { prim = `key; _ }, Mbytes h ->
      forge_micheline (Mbytes h)
    | Mprim { prim = `key_hash; _}, Mstring s ->
      let| s = pkh s in
      forge_micheline (Mbytes (Crypto.hex_of_raw s))
    | Mprim { prim = `key_hash; _ }, Mbytes h ->
      forge_micheline (Mbytes h)
    | Mprim { prim = `address; _ }, Mstring s ->
      let| s = contract s in
      forge_micheline (Mbytes (Crypto.hex_of_raw s))
    | Mprim { prim = `address; _ }, Mbytes h ->
      forge_micheline (Mbytes h)
    | Mprim { prim = `timestamp; _}, Mstring s ->
      let c = Proto.A.cal_of_str s in
      let i = Z.of_float @@ Float.round @@ CalendarLib.Calendar.to_unixfloat c in
      forge_micheline (Mint i)
    | Mprim { prim = `timestamp; _}, Mint i ->
      forge_micheline (Mint i)
    | Mprim { prim = `signature; _}, Mstring s ->
      let| s = signature s in
      forge_micheline (Mbytes (Crypto.hex_of_raw s))
    | Mprim { prim = `signature; _ }, Mbytes h ->
      forge_micheline (Mbytes h)
    | Mprim { prim = `chain_id; _}, Mstring s ->
      let| s = chain_id s in
      forge_micheline (Mbytes (Crypto.hex_of_raw s))
    | Mprim { prim = `chain_id; _ }, Mbytes h ->
      forge_micheline (Mbytes h)

    | _ -> Error `wrong_michelson_type in
  concat [ uint8 5; aux p m ]
