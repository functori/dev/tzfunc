open Proto

let (let|) = Result.bind

let prim ?(args=[]) ?(annots=[]) prim = Mprim { prim; args; annots }

let rec check_letters str i j f =
  i > j || (f str.[i] && check_letters str (i + 1) j f)

let expand_caddadr ~args ~annots s =
  let n = String.length s in
  if n > 3 && s.[0] = 'C' && s.[n-1] = 'R' &&
     check_letters s 1 (n-2) (function 'A' | 'D' -> true | _ -> false) then
    match args with
    | _ :: _ -> Error `unmacro_error
    | _ ->
      let path_annots = List.filter (function "@%" | "@%%" -> true | _ -> false) annots in
      let rec parse i acc =
        if i = 0 then Mseq acc
        else
          let annots = if i = n-2 then annots else path_annots in
          match s.[i] with
          | 'A' -> parse (i-1) ((prim `CAR ~annots) :: acc)
          | 'D' -> parse (i-1) ((prim `CDR ~annots) :: acc)
          | _ -> assert false  in
      Ok (Some (parse (n-2) []))
  else Ok None

let expand_carn ~args ~annots s = match s, args with
  | "CAR", [ Mint n ] ->
    Ok (Some (Mseq [
        prim `GET ~args:[Mint (Z.(of_int 1 + ( n * of_int 2))) ] ~annots ]))
  | _ -> Ok None

let expand_cdrn ~args ~annots s = match s, args with
  | "CDR", [ Mint n ] ->
    Ok (Some (Mseq [prim `GET ~args:[Mint Z.(n * of_int 2)] ~annots]))
  | _ -> Ok None

let extract_field_annots annots =
  List.partition (fun a -> match a.[0] with
      | '%' -> true
      | _ -> false
      | exception Invalid_argument _ -> false)
    annots

let expand_set_caddadr ~args ~annots s =
  let n = String.length s in
  if n >= 7 && String.sub s 0 5 = "SET_C" && s.[n-1] = 'R' &&
     check_letters s 5 (n-2) (function 'A' | 'D' -> true | _ -> false) then
    match args with
    | _ :: _ -> Error `unmacro_error
    | _ ->
      match extract_field_annots annots with
      | (_ :: _ :: _, _) -> Error `unmacro_error
      | (fa, a) ->
        let rec parse i acc =
          if i = 4 then acc
          else
            let annots = if i = 5 then a else [] in
            match s.[i] with
            | 'A' ->
              let acc = Mseq [
                  prim `DUP;
                  prim `DIP ~args:[
                    prim `DIP ~args:[prim `CAR ~annots:["@%%"]; acc]
                  ];
                  prim `CDR ~annots:["@%%"]; prim `SWAP;
                  prim `PAIR ~annots:("%@"::"%@":: annots) ] in
              parse (i-1) acc
            | 'D' ->
              let acc = Mseq [
                  prim `DUP;
                  prim `DIP ~args:[
                    Mseq [ prim `CDR ~annots:["@%%"]; acc ]
                  ];
                  prim `CAR ~annots:["@%%"];
                  prim `PAIR ~annots:("%@"::"%@":: annots) ] in
              parse (i-1) acc
            | _ -> assert false in
        match s.[n-2] with
        | 'A' ->
          let access_check = match fa with
            | [] -> []
            | f :: _ -> [ prim `DUP; prim `CAR ~annots:[f]; prim `DROP ] in
          let encoding = [ prim `CDR ~annots:["@%%"]; prim `SWAP ] in
          let pair = [ prim `PAIR ~annots:[match fa with [] -> "%" | _ -> "%@"] ] in
          let init = Mseq (access_check @ encoding @ pair) in
          Ok (Some (parse (n-3) init))
        | 'D' ->
          let access_check = match fa with
            | [] -> []
            | f :: _ -> [ prim `DUP; prim `CDR ~annots:[f]; prim `DROP ] in
          let encoding = [ prim `CAR ~annots:["@%%"] ] in
          let pair = [ prim `PAIR ~annots:["%@"; match fa with [] -> "%" | f :: _ -> f] ] in
          let init = Mseq (access_check @ encoding @ pair) in
            Ok (Some (parse (n-3) init))
        | _ -> assert false
  else Ok None

let expand_map_caddadr ~args ~annots s =
  let n = String.length s in
  if n >= 7 && String.sub s 0 5 = "MAP_C" && s.[n - 1] = 'R' &&
     check_letters s 5 (n - 2) (function 'A' | 'D' -> true | _ -> false) then
    let| code = match args with
      | [(Mseq _ as code)] -> Ok code
      | [_] -> Error `unmacro_error
      | [] | _ :: _ :: _ -> Error `unmacro_error in
    let| (field_annots, annots) = match extract_field_annots annots with
      | ([], annot) -> Ok (None, annot)
      | ([f], annot) -> Ok (Some f, annot)
      | (_, _) -> Error `unmacro_error in
    let rec parse i acc =
      if i = 4 then acc
      else
        let annots = if i = 5 then annots else [] in
        match s.[i] with
        | 'A' ->
          let acc = Mseq [
              prim `DUP;
              prim `DIP ~args:[Mseq [ prim `CAR ~annots:["@%%"]; acc ] ];
              prim `CDR ~annots:["@%%"];
              prim `SWAP;
              prim `PAIR ~annots:("%@" :: "%@" :: annots) ] in
          parse (i-1) acc
        | 'D' ->
          let acc = Mseq [
              prim `DUP;
              prim `DIP ~args:[Mseq [prim `CDR ~annots:["@%%"]; acc]];
              prim `CAR ~annots:["@%%"];
              prim `PAIR ~annots:("%@"::"%@"::annots) ] in
          parse (i-1) acc
        | _ -> assert false in
    let cr_annot = match field_annots with
      | None -> []
      | Some f -> ["@" ^ String.sub f 1 (String.length f - 1)] in
    match s.[n-2] with
    | 'A' ->
      let init = Mseq [
          prim `DUP;
          prim `CDR ~annots:["@%%"];
          prim `DIP ~args:[Mseq [prim `CAR ~annots:cr_annot]; code];
          prim `SWAP;
          prim `PAIR ~annots:[Option.value field_annots ~default:"%"; "%@"] ] in
      Ok (Some (parse (n-3) init))
    | 'D' ->
      let init = Mseq [
          prim `DUP;
          prim `CDR ~annots:cr_annot;
          code;
          prim `SWAP;
          prim `CAR ~annots:["@%%"];
          prim `PAIR ~annots:["%@"; Option.value field_annots ~default:"%"] ] in
      Ok (Some (parse (n-3) init))
    | _ -> assert false
  else Ok None

exception Not_a_roman

let decimal_of_roman roman =
  let arabic = ref 0 in
  let lastval = ref 0 in
  for i = String.length roman - 1 downto 0 do
    let n = match roman.[i] with
      | 'M' -> 1000
      | 'D' -> 500
      | 'C' -> 100
      | 'L' -> 50
      | 'X' -> 10
      | 'V' -> 5
      | 'I' -> 1
      | _ -> raise_notrace Not_a_roman in
    if n < !lastval then arabic := !arabic - n
    else arabic := !arabic + n;
    lastval := n
  done;
  !arabic

let dip ?annots depth instr =
  if depth = 1 then prim `DIP ~args:[instr] ?annots
  else prim `DIP ~args:[Mint (Z.of_int depth); instr] ?annots

let expand_deprecated_dxiiivp ~args ~annots s =
  let n = String.length s in
  if n > 3 && s.[0] = 'D' && s.[n-1] = 'P' then
    try
      let depth = decimal_of_roman (String.sub s 1 (n-2)) in
      match args with
      | [Mseq _ as arg] -> Ok (Some (dip ~annots depth arg))
      | _ -> Error `unmacro_error
    with Not_a_roman -> Ok None
  else Ok None

type pair_item = A | I | P of int * pair_item * pair_item

let parse_pair_substr s ~len start =
  let rec parse ?left i =
    if i = len - 1 then Error `unmacro_error
    else if s.[i] = 'P' then
      let| (next_i, l) = parse ~left:true (i + 1) in
      let| (next_i, r) = parse ~left:false next_i in
      Ok (next_i, P (i, l, r))
    else if s.[i] = 'A' && left = Some true then Ok (i + 1, A)
    else if s.[i] = 'I' && left <> Some true then Ok (i + 1, I)
    else Error `unmacro_error in
  let| (last, ast) = parse start in
  if last <> len - 1 then Error `unmacro_error else Ok ast

let unparse_pair_item ast =
  let rec unparse ast acc = match ast with
    | P (_, l, r) -> unparse r (unparse l ("P" :: acc))
    | A -> "A" :: acc
    | I -> "I" :: acc in
  String.concat "" @@ List.rev ("R" :: unparse ast [])

module IMap = Map.Make(Int)

let pappaiir_annots_pos ast annot =
  let rec find_annots_pos p_pos ast annots acc = match (ast, annots) with
    | (_, []) -> (annots, acc)
    | (P (i, left, right), _) ->
      let (annots, acc) = find_annots_pos i left annots acc in
      find_annots_pos i right annots acc
    | (A, a :: annots) ->
      let pos = match IMap.find_opt p_pos acc with
        | None -> ([a], [])
        | Some (_, cdr) -> ([a], cdr) in
      (annots, IMap.add p_pos pos acc)
    | (I, a :: annots) ->
      let pos = match IMap.find_opt p_pos acc with
        | None -> ([], [a])
        | Some (car, _) -> (car, [a]) in
      (annots, IMap.add p_pos pos acc) in
  snd (find_annots_pos 0 ast annot IMap.empty)

let expand_pappaiir ~args ~annots s =
  let n = String.length s in
  if n > 4 && s.[0] = 'P' && s.[n-1] = 'R' &&
     check_letters s 1 (n-2) (function 'P' | 'A' | 'I' -> true | _ -> false) then
    let (field_annots, annots) = extract_field_annots annots in
    match parse_pair_substr s ~len:n 0 with
    | Error _ -> Ok None
    | Ok ast ->
      let field_annots_pos = pappaiir_annots_pos ast field_annots in
      let rec parse p (depth, acc) = match p with
        | P (i, left, right) ->
          let annots = match (i, IMap.find_opt i field_annots_pos) with
            | (0, None) -> annots
            | (_, None) -> []
            | (0, Some ([], cdr_annot)) -> "%" :: cdr_annot @ annots
            | (_, Some ([], cdr_annot)) -> "%" :: cdr_annot
            | (0, Some (car_annot, cdr_annot)) -> car_annot @ cdr_annot @ annots
            | (_, Some (car_annot, cdr_annot)) -> car_annot @ cdr_annot in
          let acc =
            if depth = 0 then prim `PAIR ~annots :: acc
            else dip depth (Mseq [prim `PAIR ~annots]) :: acc in
          parse right (parse left (depth, acc))
        | A | I -> (depth + 1, acc) in
      let (_, expanded) = parse ast (0, []) in
      match args with
      | _ :: _ -> Error `unmacro_error
      | _ -> Ok (Some (Mseq expanded))
  else Ok None

let expand_unpappaiir ~args ~annots:_ s =
  let n = String.length s in
  if n > 6 && String.sub s 0 3 = "UNP" && s.[n-1] = 'R' &&
     check_letters s 3 (n-2) (function 'P' | 'A' | 'I' -> true | _ -> false) then
    let unpair = prim `UNPAIR in
    match parse_pair_substr s ~len:n 2 with
    | Error _ -> Ok None
    | Ok ast ->
      let rec parse p (depth, acc) = match p with
        | P (_i, left, right) ->
          let acc =
            if depth = 0 then unpair :: acc
            else dip depth (Mseq [unpair]) :: acc in
          parse right (parse left (depth, acc))
        | A | I -> (depth + 1, acc) in
      let (_, rev_expanded) = parse ast (0, []) in
      let expanded = Mseq (List.rev rev_expanded) in
      match args with
      | _ :: _ -> Error `unmacro_error
      | _ -> Ok (Some expanded)
  else Ok None

let expand_deprecated_duuuuup ~args ~annots s =
  let n = String.length s in
  if n > 3 && s.[0] = 'D' && s.[n-1] = 'P' &&
     check_letters s 1 (n-2) (( = ) 'U') then
    match args with
    | _ :: _ -> Error `unmacro_error
    | _ ->
      let rec parse i =
        if i = 1 then Ok (prim `DUP ~annots ~args:[Mint (Z.of_int (n-2))])
        else if s.[i] = 'U' then parse (i-1)
        else Error `unmacro_error in
      match parse (n-2) with
      | Error _ -> Ok None
      | Ok p -> Ok (Some p)
  else Ok None

let expand_compare ~args ~annots s =
  let cmp is annots =
    let is = match List.rev_map prim is with
      | Mprim {prim=i; args; _} :: r ->
        List.rev (prim i ~args ~annots :: r)
      | is -> List.rev is in
    Ok (Some (Mseq is)) in
  let ifcmp is l r annots =
    let is = List.map prim is @ [prim `IF ~args:[l; r] ~annots] in
    Ok (Some (Mseq is)) in
  match s, args with
  | "CMPEQ", [] -> cmp [`COMPARE; `EQ] annots
  | "CMPNEQ", [] -> cmp [`COMPARE; `NEQ] annots
  | "CMPLT", [] -> cmp [`COMPARE; `LT] annots
  | "CMPGT", [] -> cmp [`COMPARE; `GT] annots
  | "CMPLE", [] -> cmp [`COMPARE; `LE] annots
  | "CMPGE", [] -> cmp [`COMPARE; `GE] annots
  | "IFCMPEQ", [l; r] -> ifcmp [`COMPARE; `EQ] l r annots
  | "IFCMPNEQ", [l; r] -> ifcmp [`COMPARE; `NEQ] l r annots
  | "IFCMPLT", [l; r] -> ifcmp [`COMPARE; `LT] l r annots
  | "IFCMPGT", [l; r] -> ifcmp [`COMPARE; `GT] l r annots
  | "IFCMPLE", [l; r] -> ifcmp [`COMPARE; `LE] l r annots
  | "IFCMPGE", [l; r] -> ifcmp [`COMPARE; `GE] l r annots
  | "IFEQ", [l; r] -> ifcmp [`EQ] l r annots
  | "IFNEQ", [l; r] -> ifcmp [`NEQ] l r annots
  | "IFLT", [l; r] -> ifcmp [`LT] l r annots
  | "IFGT", [l; r] -> ifcmp [`GT] l r annots
  | "IFLE", [l; r] -> ifcmp [`LE] l r annots
  | "IFGE", [l; r] -> ifcmp [`GE] l r annots
  | ("CMPEQ" | "CMPNEQ" | "CMPLT" | "CMPGT" | "CMPLE" | "CMPGE" |
     "IFCMPEQ" | "IFCMPNEQ" | "IFCMPLT" | "IFCMPGT" | "IFCMPLE" |
     "IFCMPGE" | "IFEQ" | "IFNEQ" | "IFLT" | "IFGT" | "IFLE" | "IFGE"), _ ->
    Error `unmacro_error
  | _ -> Ok None

let expand_asserts ~args ~annots s =
  let may_rename = function
    | [] -> Mseq []
    | annots -> Mseq [prim `RENAME ~annots] in
  let fail_false ?(annots=[]) () =
    [may_rename annots; Mseq [Mseq [ prim `UNIT; prim `FAILWITH ]]] in
  let fail_true ?(annots = []) () =
    [Mseq [Mseq [ prim `UNIT; prim `FAILWITH ]]; may_rename annots] in
  match s, args, annots with
  | "ASSERT", [], [] -> Ok (Some (Mseq [prim`IF ~args:(fail_false ())]))
  | "ASSERT_NONE", [], [] -> Ok (Some (Mseq [prim `IF_NONE ~args:(fail_false ())]))
  | "ASSERT_SOME", [], annots ->
    Ok (Some (Mseq [prim `IF_NONE ~args:(fail_true () ~annots)]))
  | "ASSERT_LEFT", [], annots ->
    Ok (Some (Mseq [prim `IF_LEFT ~args:(fail_false ~annots ())]))
  | "ASSERT_RIGHT", [], annots ->
    Ok (Some (Mseq [prim `IF_LEFT ~args:(fail_true ~annots ())]))
  | ( "ASSERT" | "ASSERT_NONE" | "ASSERT_SOME" | "ASSERT_LEFT" | "ASSERT_RIGHT" ), _, _ ->
    Error `unmacro_error
  | _ when String.(length s > 7 && equal (sub s 0 7) "ASSERT_") ->
    begin match args, annots with
      | _ :: _, _ | _, _ :: _ -> Error `unmacro_error
      | _ ->
        let remaining = String.(sub s 7 (length s - 7)) in
        begin match remaining with
          | "EQ" -> Ok (Some (Mseq [ prim `EQ; prim `IF ~args:(fail_false ()) ]))
          | "NEQ" -> Ok (Some (Mseq [ prim `NEQ; prim `IF ~args:(fail_false ()) ]))
          | "LT" -> Ok (Some (Mseq [ prim `LT; prim `IF ~args:(fail_false ()) ]))
          | "LE" -> Ok (Some (Mseq [ prim `LE; prim `IF ~args:(fail_false ()) ]))
          | "GE" -> Ok (Some (Mseq [ prim `GE; prim `IF ~args:(fail_false ()) ]))
          | "GT" -> Ok (Some (Mseq [ prim `GT; prim `IF ~args:(fail_false ()) ]))
          | _ ->
            begin match expand_compare ~args:[] ~annots:[] remaining with
              | Error e -> Error e
              | Ok None -> Ok None
              | Ok (Some seq) ->
                Ok (Some (Mseq [seq; prim `IF ~args:(fail_false ())]))
            end
        end
    end
  | _ -> Ok None

let expand_if_some ~args ~annots s = match s, args with
  | "IF_SOME", [ r; l ] -> Ok (Some (Mseq [prim `IF_NONE ~args:[l; r] ~annots]))
  | "IF_SOME", _ -> Error `unmacro_error
  | _ -> Ok None

let expand_if_right ~args ~annots s = match s, args with
  | "IF_RIGHT", [r; l] -> Ok (Some (Mseq [prim `IF_LEFT ~args:[l; r] ~annots]))
  | "IF_RIGHT", _ -> Error `unmacro_error
  | _ -> Ok None

let expand_fail ~args ~annots s = match s, args, annots with
  | "FAIL", [], [] -> Ok (Some (Mseq [ prim `UNIT; prim `FAILWITH ]))
  | _ -> Ok None

let expand ~args ~annots s =
  let rec try_expansions = function
    | [] -> Error (`unknown_primitive (Some s))
    | h :: t -> match h ~args ~annots s with
      | Error e -> Error e
      | Ok None -> try_expansions t
      | Ok (Some rewritten) -> Ok rewritten in
  try_expansions [
    expand_carn;
    expand_cdrn;
    expand_caddadr;
    expand_set_caddadr;
    expand_map_caddadr;
    expand_deprecated_dxiiivp;
    expand_pappaiir;
    expand_unpappaiir;
    expand_deprecated_duuuuup;
    expand_compare;
    expand_asserts;
    expand_if_some;
    expand_if_right;
    expand_fail;
  ]
