open Ppxlib
open Ast_builder.Default

type options = {
  custom : expression option;
  map : bool;
}

let default_options = { custom = None; map = false }

let get_options ?(options=default_options) attrs =
  List.fold_left (fun options a -> match a.attr_name.txt, a.attr_payload with
      | "custom", PStr [ {pstr_desc = Pstr_eval (e, _); _} ] ->
        { options with custom = Some e }
      | "map", _ -> { options with map = true }
      | _ -> options) options attrs

let efun ~loc p e = pexp_fun ~loc Nolabel None p e

let function_name s = if s = "t" then "to_mich" else (s ^ "_to_mich")

let proto s = Ldot (Ldot (Lident "Tzfunc", "Proto"), s)
let mint ~loc e = pexp_construct ~loc {txt=proto "Mint"; loc} (Some e)
let mstring ~loc e = pexp_construct ~loc {txt=proto "Mstring"; loc} (Some e)
let mbytes ~loc e = pexp_construct ~loc {txt=proto "Mbytes"; loc} (Some e)
let mseq ~loc e = pexp_construct ~loc {txt=proto "Mseq"; loc} (Some e)
let mprim ~loc ?(args=[]) ?(annots=[]) prim =
  pexp_construct ~loc {txt=proto "Mprim"; loc} @@
  Some (pexp_record ~loc [
      {txt=Lident "prim"; loc}, pexp_variant ~loc prim None;
      {txt=Lident "args"; loc}, elist ~loc args;
      {txt=Lident "annots"; loc}, elist ~loc @@ List.map (estring ~loc) annots;
    ] None)

let rec core ?(options=default_options) c =
  let loc = c.ptyp_loc in
  let options = get_options ~options c.ptyp_attributes in
  match c.ptyp_desc, options.custom with
  | _, Some e -> e
  | Ptyp_tuple l, _ -> tuple ~loc l
  | Ptyp_constr (id, l), _ -> constr ~options id l
  | _ -> Location.raise_errorf ~loc "core_type not handled"

and constr ~options id l =
  let loc = id.loc in
  match Longident.name id.txt, l with
  | "int", _ | "Int.t", _ ->
    efun ~loc (pvar ~loc "i") @@
    mint ~loc (eapply ~loc (evar ~loc "Z.of_int") [ evar ~loc "i" ])
  | "int64", _ | "Int64.t", _ ->
    efun ~loc (pvar ~loc "i") @@
    mint ~loc (eapply ~loc (evar ~loc "Z.of_int64") [ evar ~loc "i" ])
  | "int32", _ | "Int32.t", _ ->
    efun ~loc (pvar ~loc "i") @@
    mint ~loc (eapply ~loc (evar ~loc "Z.of_int32") [ evar ~loc "i" ])
  | "Z.t", _ ->
    efun ~loc (pvar ~loc "i") @@ mint ~loc (evar ~loc "i")
  | "string", _ | "String.t", _ | "address", _ | "chain_id", _ | "key", _ | "key_hash", _
  | "signature", _ ->
    efun ~loc (pvar ~loc "s") @@ mstring ~loc (evar ~loc "s")
  | "bytes", _ | "Bytes.t", _ ->
    efun ~loc (pvar ~loc "b") @@
    eapply ~loc (evar ~loc "Tzfunc.H.mk") [
      eapply ~loc (evar ~loc "Hex.show") [
        eapply ~loc (evar ~loc "Hex.of_bytes") [ evar ~loc "b" ] ] ]
  | "bigstring", _ | "Bigstring.t", _ ->
    efun ~loc (pvar ~loc "b") @@
    eapply ~loc (evar ~loc "Tzfunc.H.mk") [
      eapply ~loc (evar ~loc "Hex.show") [
        eapply ~loc (evar ~loc "Hex.of_bigstring") [ evar ~loc "b" ] ] ]
  | "Cstruct.t", _ ->
    efun ~loc (pvar ~loc "b") @@
    eapply ~loc (evar ~loc "Tzfunc.H.mk") [
      eapply ~loc (evar ~loc "Hex.show") [
        eapply ~loc (evar ~loc "Hex.of_cstruct") [ evar ~loc "b" ] ] ]
  | "bool", _ | "Bool.t", _ ->
    efun ~loc (pvar ~loc "b") @@
    pexp_ifthenelse ~loc (evar ~loc "b")
      (mprim ~loc "True") (Some (mprim ~loc "False"))
  | "list", [ { ptyp_desc = Ptyp_tuple [k; v]; _} ] when options.map ->
    let fk = core k in
    let fv = core v in
    efun ~loc (pvar ~loc "l") @@
    mseq ~loc @@
    eapply ~loc (evar ~loc "List.map") [
      efun ~loc (ppat_tuple ~loc [ pvar ~loc "k"; pvar ~loc "v" ]) @@
      mprim ~loc ~args:[
        eapply ~loc fk [ evar ~loc "k" ];
        eapply ~loc fv [ evar ~loc "v" ]] "Elt";
      evar ~loc "l" ]
  | "list", [ c ] ->
    let f = core c in
    efun ~loc (pvar ~loc "l") @@
    mseq ~loc @@
    eapply ~loc (evar ~loc "List.map") [ f; evar ~loc "l" ]
  | "unit", _ ->
    efun ~loc (punit ~loc) (mprim ~loc "Unit")
  | "option", [ c ] ->
    let f = core c in
    pexp_function ~loc [
      case ~guard:None ~lhs:(ppat_construct ~loc {txt=Lident "Some";loc} (Some (pvar ~loc "o")))
        ~rhs:(mprim ~loc "Some" ~args:[ eapply ~loc f [ evar ~loc "o" ] ]);
      case ~guard:None ~lhs:(ppat_any ~loc) ~rhs:(mprim ~loc "None")
    ]
  | s, [] ->
    evar ~loc (function_name s)
  | _ ->  Location.raise_errorf ~loc "unknown type constructor"

and tuple ~loc l =
  efun ~loc (ppat_tuple ~loc (List.mapi (fun i _ -> pvar ~loc ("v" ^ string_of_int i)) l)) @@
  mseq ~loc @@
  elist ~loc @@ List.mapi (fun i c ->
      let f = core c in
      eapply ~loc f [ evar ~loc ("v" ^ string_of_int i) ]) l

let record ~loc ?(options=default_options) l =
  let pat = ppat_record ~loc (List.map (fun pld ->
      let loc = pld.pld_loc in
      {txt = Lident pld.pld_name.txt; loc}, pvar ~loc pld.pld_name.txt) l) Closed in
  efun ~loc pat @@
  mseq ~loc @@
  elist ~loc @@ List.map (fun pld ->
      let options = get_options ~options pld.pld_attributes in
      let f = core ~options pld.pld_type in
      eapply ~loc f [
        evar ~loc pld.pld_name.txt ]) l

type tree =
  | Leaf
  | Node of (tree * tree)

let rec int_to_tree acc i =
  if i = 1 then acc
  else
    match acc with
    | Leaf -> int_to_tree (Node (Leaf, Leaf)) (i-1)
    | Node (left, right) when left = right -> int_to_tree (Node (Node (left, right), Leaf)) (i-1)
    | Node (left, right) -> int_to_tree (Node (left, int_to_tree right 2)) (i-1)

let rec list_of_tree = function
  | Leaf -> [[]]
  | Node (left, right) ->
    let acc_left = List.map (fun l -> "Left" :: l) (list_of_tree left) in
    let acc_right = List.map (fun l -> "Right" :: l) (list_of_tree right) in
    acc_left @ acc_right

let rec lr_expr ~loc e = function
  | [] -> e
  | h :: t ->  mprim ~loc h ~args:[ lr_expr ~loc e t ]

let variant ~loc l =
  let lr = list_of_tree @@ int_to_tree Leaf (List.length l) in
  let cases = List.map2 (fun pcd lr ->
      let options = get_options pcd.pcd_attributes in
      let loc = pcd.pcd_loc in
      match pcd.pcd_args with
      | Pcstr_tuple [] ->
        let lhs = ppat_construct ~loc {txt = Lident pcd.pcd_name.txt; loc} None in
        let rhs = lr_expr ~loc (mprim ~loc "Unit") lr in
        case ~guard:None ~lhs ~rhs
      | Pcstr_tuple l ->
        let f = match l with [c] -> core ~options c | l -> tuple ~loc l in
        let e = eapply ~loc f [ evar ~loc "a" ] in
        let lhs = ppat_construct ~loc {txt = Lident pcd.pcd_name.txt; loc} (Some (pvar ~loc "a")) in
        let rhs = lr_expr ~loc e lr in
        case ~guard:None ~lhs ~rhs
      | Pcstr_record l ->
        let e = eapply ~loc (record ~loc ~options l) [ evar ~loc "a" ] in
        let lhs = ppat_construct ~loc {txt = Lident pcd.pcd_name.txt; loc} (Some (pvar ~loc "a")) in
        let rhs = lr_expr ~loc e lr in
        case ~guard:None ~lhs ~rhs
    ) l lr in
  pexp_function ~loc cases

let ptype ~rec_flag t =
  let loc = t.ptype_loc in
  let name = t.ptype_name.txt in
  let expr = match t.ptype_kind, t.ptype_manifest with
    | Ptype_abstract, Some m -> core m
    | Ptype_variant l, _ -> variant ~loc l
    | Ptype_record l, _ -> record ~loc l
    | _ -> Location.raise_errorf ~loc "type_declaration not handled" in
  let pat = ppat_constraint ~loc (pvar ~loc (function_name name))
         (ptyp_arrow ~loc Nolabel (ptyp_constr ~loc {txt=Lident name; loc} [])
            (ptyp_constr ~loc {txt=proto "micheline"; loc} [])) in
  let v = value_binding ~loc ~pat ~expr in
  pstr_value ~loc rec_flag [ v ]

let str_gen ~loc:_ ~path:_ (rec_flag, l) debug =
  let rec_flag =
    if rec_flag = Nonrecursive then Nonrecursive
    else if List.length l = 1 then Nonrecursive else Recursive in
  let s = List.map (ptype ~rec_flag) l in
  if debug then (
    Pprintast.structure Format.std_formatter s;
    Format.print_newline ()
  );
  s

let sig_gen ~loc:_ ~path:_ (_, l) =
  List.map (fun t ->
      let loc = t.ptype_loc in
      let name = t.ptype_name.txt in
      let vd = value_description ~loc ~name:{txt=function_name name; loc} ~prim:[]
          ~type_:(ptyp_arrow ~loc Nolabel (ptyp_constr ~loc {txt=Lident name; loc} [])
                    (ptyp_constr ~loc {txt=proto "micheline"; loc} [])) in
      psig_value ~loc vd
    ) l

let () =
  let args_str = Deriving.Args.(empty +> flag "debug") in
  let str_type_decl = Deriving.Generator.make args_str str_gen in
  let sig_type_decl = Deriving.(Generator.make Args.empty sig_gen) in
  Deriving.ignore @@ Deriving.add "micheline" ~str_type_decl ~sig_type_decl
