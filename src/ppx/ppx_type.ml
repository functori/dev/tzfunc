open Ppxlib
open Micheline_parser
open Ast_builder.Default

let resolve_otype ~loc = function
  | `s c -> c
  | `p l ->
    let no_names = List.for_all (fun (n, _) -> Option.is_none n) l in
    if no_names then ptyp_tuple ~loc @@ List.map snd l
    else ptyp_object ~loc (List.mapi (fun i (n, f) ->
        let txt = match n with None -> "_" ^ (string_of_int i) | Some n -> n in
        otag ~loc {txt; loc} f
      ) l) Closed
  | `o l ->
    ptyp_variant ~loc (List.mapi (fun i (n, f) ->
        let txt = match n with None -> string_of_int i | Some n -> n in
        rtag ~loc {txt; loc} false [ f ]) l) Closed None

let rec otype_aux ~loc t =
  let name =
    try let s = List.hd t.annots in Some (String.sub s 1 (String.length s - 1))
    with _ -> None in
  let e = match t.prim, t.args with
    | ("address" | "chain_id" | "key" | "key_hash" | "string" | "tx_rollup_l2_address"), [] ->
      `s [%type: string]
    | "bool", [] -> `s [%type: bool]
    | ("bytes" | "chest" | "chest_key" | "bls12_381_g1" | "bls12_381_g2"), [] ->
      `s [%type: Proto.hex]
    | ("int" | "mutez" | "nat" | "bls12_381_fr"), [] ->
      `s [%type: Proto.A.zarith]
    | "timestamp", [] -> `s [%type: Proto.A.timestamp]
    | "unit", [] -> `s [%type: unit]
    | "option", [ t ] ->
      let f = resolve_otype ~loc @@ snd @@ otype_aux ~loc t in
      `s [%type: [%t f] option]
    | "list", [ t ] | "set", [ t ] ->
      let f = resolve_otype ~loc @@ snd @@ otype_aux ~loc t in
      `s [%type: [%t f] list]
    | "or", [ tl; tr ] ->
      begin match otype_aux ~loc tl, otype_aux ~loc tr with
        | (None, `o ll), (None, `o lr) -> `o (ll @ lr)
        | (nl, fl), (None, `o lr) -> `o ((nl, resolve_otype ~loc fl) :: lr)
        | (None, `o ll), (nr, fr) -> `o (ll @ [ nr, resolve_otype ~loc fr ])
        | (nl, fl), (nr, fr) ->
          `o [ nl, resolve_otype ~loc fl; nr, resolve_otype ~loc fr ]
      end
    | "map", [ tk; tv ] ->
      let fk = resolve_otype ~loc @@ snd @@ otype_aux ~loc tk in
      let fv = resolve_otype ~loc @@ snd @@ otype_aux ~loc tv in
      `s [%type: ([%t fk] * [%t fv]) list]
    | "big_map", [ tk; tv ] ->
      let fk = resolve_otype ~loc @@ snd @@ otype_aux ~loc tk in
      let fv = resolve_otype ~loc @@ snd @@ otype_aux ~loc tv in
      `s [%type: [ `list of ([%t fk] * [%t fv]) list | `id of Proto.A.zarith ]]
    | "pair", [ t1; t2 ] ->
      let name1, f1 = otype_aux ~loc t1 in
      let f1 = resolve_otype ~loc f1 in
      begin match otype_aux ~loc t2 with
        | (Some _) as name2, f2 -> `p [ name1, f1; name2, resolve_otype ~loc f2 ]
        | _name2, `p l -> `p ((name1, f1) :: l)
        | name2, f2 -> `p [ name1, f1; name2, resolve_otype ~loc f2 ]
      end
    | "pair", (t1 :: args) ->
      snd @@ otype_aux ~loc {prim="pair"; args=[t1; {prim="pair"; args; annots=[]}]; annots=t.annots}
    | ("operation" | "contract" | "lambda" | "ticket" | "sapling_state"
      | "sapling_transaction" | "sapling_transaction_deprecated"), _ ->
      `s [%type: Proto.micheline]
    | _ -> Location.raise_errorf "primitive type not handled %S(%d)" t.prim (List.length t.args) in
  name, e

let otype ~loc t = resolve_otype ~loc @@ snd @@ otype_aux ~loc t
