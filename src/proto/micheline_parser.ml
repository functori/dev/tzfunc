type token =
  | String of string
  | Bytes of string
  | Int of string
  | Ident of string
  | Annot of string
  | Comment of string
  | Eol_comment of string
  | Semi
  | Open_paren | Close_paren
  | Open_brace | Close_brace

type value =
  [ `Null
  | `Bool of bool
  | `Float of float
  | `String of string
  | `A of value list
  | `O of (string * value) list ]

type mode =
  | Toplevel of value list
  | Expression of value option
  | Sequence of token * value list
  | Unwrapped of string * value list * string list
  | Wrapped of token * string * value list * string list

let max_annot_length = 255

let tokenize source =
  let decoder = Uutf.decoder ~encoding:`UTF_8 (`String source) in
  let here () = Uutf.decoder_byte_count decoder in
  let stack = ref [] in
  let next () =
    match !stack with
    | charloc :: charlocs ->
      stack := charlocs ;
      charloc
    | [] ->
      let loc = here () in
      match Uutf.decode decoder with
      | `Await -> failwith "await"
      | `Malformed _s -> failwith "invalid utf8 sequence"
      | `Uchar _ | `End as other -> other, loc in
  let back charloc =
    stack := charloc :: !stack in
  let uchar_to_char c =
    if Uchar.is_char c then
      Some (Uchar.to_char c)
    else
      None in
  let allowed_ident_char c =
    match uchar_to_char c with
    | Some ('a'..'z' | 'A'..'Z' | '_' | '0'..'9') -> true
    | Some _ | None -> false in
  let allowed_annot_char c =
    match uchar_to_char c with
    | Some ('a'..'z' | 'A'..'Z' | '_' | '.' | '%' | '@' | '0'..'9') -> true
    | Some _ | None -> false in
  let rec skip acc =
    match next () with
    | `End, _ -> List.rev acc
    | `Uchar c, start ->
      begin match uchar_to_char c with
        | Some ('a'..'z' | 'A'..'Z') -> ident acc start (fun s _ -> Ident s)
        | Some ('@' | ':' | '$' | '&' | '%' | '!' | '?') ->
          annot acc start
            (fun str _ ->
               if String.length str > max_annot_length then failwith "annotation length"
               else Annot str )
          | Some '-' ->
          begin match next () with
            | `End, _ -> failwith "unterminated integer"
            | `Uchar c, _ ->
              begin match uchar_to_char c with
                | Some '0' -> base acc start
                | Some ('1'..'9') -> integer acc start
                | Some _ | None -> failwith "unterminated integer"
              end
          end
        | Some '0' -> base acc start
        | Some ('1'..'9') -> integer acc start
        | Some (' ' | '\n') -> skip acc
        | Some ';' -> skip (Semi :: acc)
        | Some '{' -> skip (Open_brace :: acc)
        | Some '}' -> skip (Close_brace :: acc)
        | Some '(' -> skip (Open_paren :: acc)
        | Some ')' -> skip (Close_paren :: acc)
        | Some '"' -> string acc [] start
        | Some '#' -> eol_comment acc start
        | Some '/' ->
          begin match next () with
            | `Uchar c, _ when Uchar.equal c (Uchar.of_char '*') ->
              comment acc start 0
            | _ -> failwith "unexpected character"
          end
        | Some _ | None -> failwith "unexpected character"
      end
  and base acc start =
    match next () with
    | (`Uchar c, _) as charloc ->
      begin match uchar_to_char c with
        | Some ('0'.. '9') -> integer acc start
        | Some 'x' -> bytes acc start
        | Some ('a'..'w' | 'y' | 'z' | 'A'..'Z') -> failwith "missing break after number"
        | Some _ | None ->
          back charloc ;
          skip ((Int "0") :: acc)
      end
    | _ as other ->
      back other ;
      skip ((Int "0") :: acc)
  and integer acc start =
    let tok stop =
      let value = String.sub source start (stop - start) in
      Int value in
    match next () with
    | (`Uchar c, stop) as charloc ->
      begin match Uchar.to_char c with
        | ('0'.. '9') -> integer acc start
        | ('a'..'z' | 'A'..'Z') -> failwith "missing break after number"
        | _ ->
          back charloc ;
          skip (tok stop :: acc)
      end
    | (`End, stop) as other ->
      back other ;
      skip (tok stop :: acc)
  and bytes acc start =
    let tok stop =
      let value = String.sub source start (stop - start) in
      Bytes value in
    match next () with
    | (`Uchar c, stop) as charloc ->
      begin match Uchar.to_char c with
        | ('0'..'9' | 'a'..'f' | 'A'..'F') -> bytes acc start
        | ('g'..'z' | 'G'..'Z') -> failwith "missing break after number"
        | _ ->
          back charloc ;
          skip (tok stop :: acc)
      end
    | (`End, stop) as other ->
      back other ;
      skip (tok stop :: acc)
  and string acc sacc start =
    let tok () = String (String.concat "" (List.rev sacc)) in
    match next () with
    | `End, _ -> failwith "unterminated string"
    | `Uchar c, stop ->
      match uchar_to_char c with
      | Some '"' -> skip (tok () :: acc)
      | Some ('\n' | '\r') -> failwith "unterminated string"
      | Some '\\' ->
        begin match next () with
          | `End, _ -> failwith "unterminated string"
          | `Uchar c, _ ->
            match uchar_to_char c with
            | Some '"' -> string acc ("\"" :: sacc) start
            | Some 'r' -> string acc ("\r" :: sacc) start
            | Some 'n' -> string acc ("\n" :: sacc) start
            | Some 't' -> string acc ("\t" :: sacc) start
            | Some 'b' -> string acc ("\b" :: sacc) start
            | Some '\\' -> string acc ("\\" :: sacc) start
            | Some _ | None -> failwith "undefined escape sequence"
        end
      | Some _ | None ->
        let byte = Uutf.decoder_byte_count decoder in
        let s = String.sub source stop (byte - stop) in
        string acc (s :: sacc) start
  and generic_ident allow_char acc start (ret : string -> int -> token) =
    let tok stop =
      let name = String.sub source start (stop - start) in
      (ret name stop) in
    match next () with
    | (`Uchar c, stop) as charloc ->
      if allow_char c then
        generic_ident allow_char acc start ret
      else begin
        back charloc ;
        skip (tok stop :: acc)
      end
    | (_, stop) as other ->
      back other ;
      skip (tok stop :: acc)
  and ident acc start ret = generic_ident allowed_ident_char acc start ret
  and annot acc start ret = generic_ident allowed_annot_char acc start ret
  and comment acc start lvl =
    match next () with
    | `End, _ -> failwith "unterminated comment"
    | `Uchar c, _ ->
      begin match uchar_to_char c with
        | Some '*' ->
          begin match next () with
            | `Uchar c, _ when Uchar.equal c (Uchar.of_char '/') ->
              if lvl = 0 then
                let stop = here () in
                let text =
                  String.sub source start (stop - start) in
                skip ((Comment text) :: acc)
              else
                comment acc start (lvl - 1)
            | other ->
              back other ;
              comment acc start lvl
          end
        | Some '/' ->
          begin match next () with
            | `Uchar c, _ when Uchar.equal c (Uchar.of_char '*') ->
              comment acc start (lvl + 1)
            | other ->
              back other ;
              comment acc start lvl
          end
        | Some _ | None -> comment acc start lvl
      end
  and eol_comment acc start =
    let tok stop =
      let text = String.sub source start (stop - start) in
      Eol_comment text in
    match next () with
    | `Uchar c, stop ->
      begin match uchar_to_char c with
        | Some '\n' -> skip (tok stop :: acc)
        | Some _ | None -> eol_comment acc start
      end
    | (_, stop) as other ->
      back other ;
      skip (tok stop :: acc) in
  let tokens = skip [] in
  tokens

let rec annots = function
  | Annot annot :: rest ->
    let annots, rest = annots rest in
    annot :: annots, rest
  | rest -> [], rest

let pop_mode = function
  | [] -> assert false
  | _ :: rest -> rest

let fill_mode result = function
  | [] -> assert false
  | Expression _ :: _ :: _ -> assert false
  | Expression (Some _) :: [] -> assert false
  | Toplevel _ :: _ :: _ -> assert false
  | Expression None :: []  ->
    Expression (Some result) :: []
  | Toplevel exprs :: [] ->
    Toplevel (result :: exprs) :: []
  | Sequence (token, exprs) :: rest ->
    Sequence (token, result :: exprs) :: rest
  | Wrapped (token, name, exprs, annot) :: rest ->
    Wrapped (token, name, result :: exprs, annot) :: rest
  | Unwrapped (name, exprs, annot) :: rest ->
    Unwrapped (name, result :: exprs, annot) :: rest

let rec parse_aux tokens stack =
  match stack, tokens with
  | [], _
  | [ Wrapped _ ], _
  | [ Unwrapped _ ], _
  | Unwrapped _ :: Unwrapped _ :: _, _
  | Unwrapped _ :: Wrapped _ :: _, _
  | Toplevel _ :: _ :: _, _
  | Expression _ :: _ :: _, _ -> failwith "cannot parse micheline expression"
  | Expression (Some result) :: _, [] -> [ result ]
  | Expression (Some _) :: _, _token :: _rem -> failwith "unexpected token"
  | Expression None :: _, [] -> failwith "empty expression"
  | Toplevel [ `A exprs ] :: [], [] -> exprs
  | Toplevel exprs :: [], [] -> List.rev exprs
  | _, (Eol_comment _ | Comment _) :: rest -> parse_aux rest stack
  | (Expression None | Sequence _ | Toplevel _) :: _,
    (Int _ | String _ | Bytes _ as token) :: (Eol_comment _ | Comment _) :: rest
  | (Wrapped _ | Unwrapped _) :: _,
    (Open_paren as token) :: (Eol_comment _ | Comment _) :: rest ->
    parse_aux (token :: rest) stack
  | (Wrapped _ | Unwrapped _) :: _ ,
    (Open_paren as _token) :: (Open_paren | Open_brace) :: _rem
  | Unwrapped _ :: Expression _ :: _ ,
    (Semi | Close_brace | Close_paren as _token) :: _rem
  | Expression None :: _ ,
    (Semi | Close_brace | Close_paren | Open_paren as _token) :: _rem ->
    failwith "unexpected token"
  | (Sequence _ | Toplevel _) :: _ ,
    Semi :: (Semi as _token) :: _rem ->
    failwith "extra token"
  | (Wrapped _ | Unwrapped _) :: _ ,
    Open_paren :: (Int _ | String _ | Bytes _ | Annot _ | Close_paren as _token) :: _rem
  | (Expression None | Sequence _ | Toplevel _) :: _,
    (Int _ | String _ | Bytes _) :: (Ident _ | Int _ | String _ | Bytes _ | Annot _ | Close_paren | Open_paren | Open_brace as _token) :: _rem
  | Unwrapped (_, _, _) :: Toplevel _ :: _, (Close_brace as _token) :: _rem
  | Unwrapped (_, _, _) :: _, (Close_paren as _token) :: _rem
  | Toplevel _ :: [], (Close_paren as _token) :: _rem
  | Toplevel _ :: [], (Open_paren as _token) :: _rem
  | Toplevel _ :: [], (Close_brace as _token) :: _rem
  | Sequence _ :: _, (Open_paren as _token) :: _rem
  | Sequence _ :: _, (Close_paren as _token) :: _rem
  | (Wrapped _ | Unwrapped _) :: _,
    (Open_paren as _token) :: ((Close_brace | Semi) :: _ | [] as _rem)
  | _, (Annot _ as _token) :: _rem -> failwith "unexpected token"
  | Wrapped (_token, _, _, _) :: _, ([] | (Close_brace | Semi) :: _) ->
    failwith "unclosed token"
  | (Sequence (_token, _) :: _ | Unwrapped _ :: Sequence (_token, _) :: _), [] ->
    failwith "unclosed token"
  (* Valid states *)
  | (Toplevel _ | Sequence (_, _)) :: _ , Ident name :: (Annot _ :: _ as rest) ->
    let annots, rest = annots rest in
    let mode = Unwrapped (name, [], annots) in
    parse_aux rest (mode :: stack)
  | (Expression None | Toplevel _ | Sequence (_, _)) :: _ ,
    Ident name :: rest ->
    let mode = Unwrapped (name, [], []) in
    parse_aux rest (mode :: stack)
  | (Unwrapped _ | Wrapped _) :: _,
    Int value :: rest
  | (Expression None | Sequence _ | Toplevel _) :: _,
    Int value :: ([] | (Semi | Close_brace) :: _ as rest) ->
    let expr = `O ["int", `String value] in
    parse_aux rest (fill_mode expr stack)
  | (Unwrapped _ | Wrapped _) :: _,
    String contents :: rest
  | (Expression None | Sequence _ | Toplevel _) :: _,
    String contents :: ([] | (Semi | Close_brace) :: _ as rest) ->
    let expr = `O ["string", `String contents] in
    parse_aux rest (fill_mode expr stack)
  | (Unwrapped _ | Wrapped _) :: _,
    Bytes contents :: rest
  | (Expression None | Sequence _ | Toplevel _) :: _,
    Bytes contents :: ([] | (Semi | Close_brace) :: _ as rest) ->
    if String.length contents mod 2 <> 0 then failwith "odd lengthed bytes"
    else
      let expr = `O ["bytes", `String (String.sub contents 2 (String.length contents - 2))] in
      parse_aux rest (fill_mode expr stack)
  | Sequence (_, exprs) :: _ ,
    Close_brace :: rest ->
    let expr = `A (List.rev exprs) in
    parse_aux rest (fill_mode expr (pop_mode stack))
  | (Sequence _ | Toplevel _) :: _ ,
    Semi :: rest ->
    parse_aux rest stack
  | Unwrapped (name, exprs, annots) :: Expression _ :: _,
    ([] as rest)
  | Unwrapped (name, exprs, annots) :: Toplevel _ :: _,
    (Semi :: _ | [] as rest)
  | Unwrapped (name, exprs, annots) :: Sequence _ :: _ ,
    ((Close_brace | Semi) :: _ as rest)
  | Wrapped (_, name, exprs, annots) :: _ ,
    Close_paren :: rest ->
    let expr = `O [
        "prim", `String name; "args", `A (List.rev exprs);
        "annots", `A (List.map (fun s -> `String s) annots) ] in
    parse_aux rest (fill_mode expr (pop_mode stack))
  | (Wrapped _ | Unwrapped _) :: _ ,
    (Open_paren as token) :: Ident name :: (Annot _ :: _ as rest) ->
    let annots, rest = annots rest in
    let mode = Wrapped (token, name, [], annots) in
    parse_aux rest (mode :: stack)
  | (Wrapped _ | Unwrapped _) :: _ ,
    (Open_paren as token) :: Ident name :: rest ->
    let mode = Wrapped (token, name, [], []) in
    parse_aux rest (mode :: stack)
  | (Wrapped _ | Unwrapped _) :: _ ,
    Ident name :: rest ->
    let expr = `O ["prim", `String name] in
    parse_aux rest (fill_mode expr stack)
  | (Wrapped _ | Unwrapped _ | Toplevel _ | Sequence _ | Expression None) :: _ ,
    (Open_brace as token) :: rest ->
    let mode = Sequence (token, []) in
    parse_aux rest (mode :: stack)

let parse s =
  let tokens = tokenize s in
  let result = match tokens with
    | Open_paren :: Ident name :: Annot annot :: rest ->
      let annots, rest = annots rest in
      let mode = Wrapped (Open_paren, name, [], annot :: annots) in
      parse_aux rest [ mode ; Expression None ]
    | Open_paren :: Ident name :: rest ->
      let mode = Wrapped (Open_paren, name, [], []) in
      parse_aux rest [ mode ; Expression None ]
    | _ ->
      parse_aux tokens [ Expression None ] in
  match result with
  | [ single ] -> single
  | _ -> assert false

type token_type = Ident of string | Annot of string | Open_paren | Close_paren

let tokenize_type source =
  let decoder = Uutf.decoder ~encoding:`UTF_8 (`String source) in
  let here () = Uutf.decoder_byte_count decoder in
  let stack = ref [] in
  let next () = match !stack with
    | charloc :: charlocs -> stack := charlocs; charloc
    | [] ->
      let loc = here () in
      match Uutf.decode decoder with
      | `Await -> failwith "await"
      | `Malformed _s -> failwith "invalid utf8 sequence"
      | `Uchar _ | `End as other -> other, loc in
  let back charloc = stack := charloc :: !stack in
  let uchar_to_char c =
    if Uchar.is_char c then Some (Uchar.to_char c)
    else None in
  let allowed_ident_char c =
    match uchar_to_char c with
    | Some ('a'..'z' | 'A'..'Z' | '_' | '0'..'9') -> true
    | Some _ | None -> false in
  let allowed_annot_char c =
    match uchar_to_char c with
    | Some ('a'..'z' | 'A'..'Z' | '_' | '.' | '%' | '@' | '0'..'9') -> true
    | Some _ | None -> false in
  let rec skip acc = match next () with
    | `End, _ -> List.rev acc
    | `Uchar c, start ->
      begin match uchar_to_char c with
        | Some ('a'..'z' | 'A'..'Z') -> ident acc start (fun s -> Ident s)
        | Some ('@' | ':' | '$' | '&' | '%' | '!' | '?') ->
          annot acc start
            (fun str ->
               if String.length str > max_annot_length then failwith "annotation length"
               else Annot str )
        | Some (' ' | '\n') -> skip acc
        | Some '(' -> skip (Open_paren :: acc)
        | Some ')' -> skip (Close_paren :: acc)
        | Some _ | None -> failwith "unexpected character"
      end
  and generic_ident allow_char acc start (ret : string -> token_type) =
    let tok stop = ret @@ String.sub source start (stop - start) in
    match next () with
    | (`Uchar c, stop) as charloc ->
      if allow_char c then generic_ident allow_char acc start ret
      else (back charloc; skip (tok stop :: acc))
    | (_, stop) as other -> back other; skip (tok stop :: acc)
  and ident acc start ret = generic_ident allowed_ident_char acc start ret
  and annot acc start ret = generic_ident allowed_annot_char acc start ret in
  let tokens = skip [] in
  tokens

type micheline_type = {
  prim : string;
  args: micheline_type list;
  annots: string list;
}

type mode_type =
  | Expression of micheline_type option
  | Unwrapped of string * micheline_type list * string list
  | Wrapped of token_type * string * micheline_type list * string list

let rec annots = function
  | Annot annot :: rest ->
    let annots, rest = annots rest in
    annot :: annots, rest
  | rest -> [], rest

let fill_mode result = function
  | [] | Expression _ :: _ :: _ | Expression (Some _) :: [] -> assert false
  | Expression None :: []  -> Expression (Some result) :: []
  | Wrapped (token, name, exprs, annot) :: rest ->
    Wrapped (token, name, result :: exprs, annot) :: rest
  | Unwrapped (name, exprs, annot) :: rest ->
    Unwrapped (name, result :: exprs, annot) :: rest

let rec parse_aux tokens stack =
  match stack, tokens with
  | [], _ | [ Wrapped _ ], _ | [ Unwrapped _ ], _ | Unwrapped _ :: Unwrapped _ :: _, _
  | Unwrapped _ :: Wrapped _ :: _, _ | Expression _ :: _ :: _, _ ->
    failwith "cannot parse micheline expression"
  | Expression None :: _, [] -> failwith "empty expression"
  | Expression (Some _) :: _, _token :: _rem
  | (Wrapped _ | Unwrapped _) :: _ , (Open_paren as _token) :: Open_paren :: _rem
  | Unwrapped _ :: Expression _ :: _ , (Close_paren as _token) :: _rem
  | Expression None :: _ , (Close_paren | Open_paren as _token) :: _rem
  | (Wrapped _ | Unwrapped _) :: _ , Open_paren :: (Annot _ | Close_paren as _token) :: _rem
  | (Wrapped _ | Unwrapped _) :: _, (Open_paren as _token) :: ([] as _rem)
  | _, (Annot _ as _token) :: _rem -> failwith "unexpected token"
  | Wrapped (_token, _, _, _) :: _, [] -> failwith "unclosed token"
  | Expression (Some result) :: _, [] -> result
  | Expression None :: _ , Ident name :: rest ->
    let mode = Unwrapped (name, [], []) in
    parse_aux rest (mode :: stack)
  | Unwrapped (prim, exprs, annots) :: Expression _ :: _, ([] as rest)
  | Wrapped (_, prim, exprs, annots) :: _, Close_paren :: rest ->
    let expr = {prim; args = List.rev exprs; annots} in
    parse_aux rest (fill_mode expr (pop_mode stack))
  | (Wrapped _ | Unwrapped _) :: _ ,
    (Open_paren as token) :: Ident name :: (Annot _ :: _ as rest) ->
    let annots, rest = annots rest in
    let mode = Wrapped (token, name, [], annots) in
    parse_aux rest (mode :: stack)
  | (Wrapped _ | Unwrapped _) :: _ ,
    (Open_paren as token) :: Ident name :: rest ->
    let mode = Wrapped (token, name, [], []) in
    parse_aux rest (mode :: stack)
  | (Wrapped _ | Unwrapped _) :: _ ,
    Ident prim :: rest ->
    let expr = {prim; args=[]; annots=[]} in
    parse_aux rest (fill_mode expr stack)

let parse_type s =
  let tokens = tokenize_type s in
  match tokens with
  | Open_paren :: Ident name :: Annot annot :: rest ->
    let annots, rest = annots rest in
    let mode = Wrapped (Open_paren, name, [], annot :: annots) in
      parse_aux rest [ mode ; Expression None ]
  | Open_paren :: Ident name :: rest ->
    let mode = Wrapped (Open_paren, name, [], []) in
    parse_aux rest [ mode ; Expression None ]
  | _ -> parse_aux tokens [ Expression None ]
