(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2021 Functori - contact@functori.com                        *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

module Encoding = Encoding

type 'a encoding = 'a Encoding.encoding = {
  json : 'a Json_encoding.encoding;
  bin : 'a Binary.Encoding.encoding;
}

type hex = Crypto.H.t [@encoding Encoding.hex]
[@@deriving encoding {title="hex"}]

[@@@jsoo
  open Ezjs_min
  class type hex_jsoo = js_string
  let hex_to_jsoo (s : hex) = string (s :> string)
  let hex_of_jsoo (js : hex_jsoo t) = Crypto.H.mk @@ to_string js
  let hex_jsoo_conv = hex_to_jsoo, hex_of_jsoo
]

module A = struct
  type uint32 = (int32 [@number]) [@encoding Encoding.uint32] [@@deriving encoding {title="uint32"}, jsoo]
  type uint64 = int64 [@encoding Encoding.uint64] [@@deriving encoding {title="uint64"}]
  [@@@jsoo
    class type uint64_jsoo = js_string
    let uint64_to_jsoo i = string (Int64.to_string i)
    let uint64_of_jsoo js = Int64.of_string (to_string js)
    let uint64_jsoo_conv = uint64_to_jsoo, uint64_of_jsoo
  ]

  type uzarith = Z.t [@encoding Encoding.uzarith] [@@deriving encoding {title="uzarith"}]
  type zarith = Z.t [@encoding Encoding.zarith] [@@deriving encoding {title="zarith"}]
  [@@@jsoo
    class type zarith_jsoo = js_string
    let zarith_to_jsoo i = string (Z.to_string i)
    let zarith_of_jsoo js = Z.of_string (to_string js)
    let zarith_jsoo_conv = zarith_to_jsoo, zarith_of_jsoo
  ]

  let cal_to_str ?(ms=false) date =
    let s = CalendarLib.Printer.Calendar.sprint "%Y-%m-%dT%H:%M:%S" date in
    if ms then s ^ ".000Z" else s ^ "Z"
  let cal_of_str s =
    try CalendarLib.Printer.Calendar.from_fstring "%Y-%m-%dT%H:%M:%SZ" s
    with _ ->
    try CalendarLib.Calendar.from_unixfloat (Float.of_string s)
    with _ ->
      if String.length s < 5 then failwith "cannot parse timestamp"
      else
        let s = String.sub s 0 (String.length s - 5) in
        CalendarLib.Printer.Calendar.from_fstring "%Y-%m-%dT%H:%M:%S" s

  type timestamp = CalendarLib.Calendar.t
  let timestamp_enc = Encoding.conv cal_to_str cal_of_str Encoding.string
  [@@@jsoo
    class type timestamp_jsoo = date
    let timestamp_to_jsoo c = new%js date_fromTimeValue (float (1000. *. CalendarLib.Calendar.to_unixfloat c))
    let timestamp_of_jsoo js = CalendarLib.Calendar.from_unixfloat ((to_float js##getTime) /. 1000.)
    let timestamp_jsoo_conv = timestamp_to_jsoo, timestamp_of_jsoo
  ]
  type block_hash = string [@encoding Encoding.block_hash] [@@deriving encoding {title="block_hash"}, jsoo]
  type operation_hash = string [@encoding Encoding.operation_hash] [@@deriving encoding {title="operation_hash"}, jsoo]
  type operations_hash = string [@encoding Encoding.operations_hash] [@@deriving encoding {title="operations_hash"}, jsoo]
  type contract = string [@encoding Encoding.contract] [@@deriving encoding {title="contract"}, jsoo]
  type pkh = string [@encoding Encoding.pkh] [@@deriving encoding {title="pkh"}, jsoo]
  type pk = string [@encoding Encoding.pk] [@@deriving encoding {title="pk"}, jsoo]
  type protocol_hash = string [@encoding Encoding.protocol_hash] [@@deriving encoding {title="protocol_hash"}, jsoo]
  type context_hash = string [@encoding Encoding.context_hash] [@@deriving encoding {title="context_hash"}, jsoo]
  type nonce_hash = string [@encoding Encoding.nonce_hash] [@@deriving encoding {title="nonce_hash"}, jsoo]
  type chain_id = string [@encoding Encoding.chain_id] [@@deriving encoding {title="chain_id"}, jsoo]
  type signature = string [@encoding Encoding.signature] [@@deriving encoding {title="signature"}, jsoo]
  type script_expr_hash = string [@encoding Encoding.script_expr_hash] [@@deriving encoding {title="script_expr_hash"}, jsoo]
  type tx_rollup_hash = string [@encoding Encoding.tx_rollup_hash] [@@deriving encoding {title="tx_rollup_hash"}, jsoo]
  type tx_inbox_hash = string [@encoding Encoding.tx_inbox_hash] [@@deriving encoding {title="tx_inbox_hash"}, jsoo]
  type tx_message_result_hash = string [@encoding Encoding.tx_message_result_hash] [@@deriving encoding {title="tx_message_result_hash"}, jsoo]
  type tx_commitment_hash = string [@encoding Encoding.tx_commitment_hash] [@@deriving encoding {title="tx_commitment_hash"}, jsoo]
  type sc_rollup_hash = string [@encoding Encoding.sc_rollup_hash] [@@deriving encoding {title="sc_rollup_hash"}, jsoo]
  type sc_commitment_hash = string [@encoding Encoding.sc_commitment_hash] [@@deriving encoding {title="sc_commitment_hash"}, jsoo]
  type sc_state_hash = string [@encoding Encoding.sc_state_hash] [@@deriving encoding {title="sc_state_hash"}, jsoo]

end

(* michelson *)

type primitive = [
  | `parameter
  | `storage
  | `code
  | `False
  | `Elt
  | `Left
  | `None
  | `Pair
  | `Right
  | `Some
  | `True
  | `Unit
  | `PACK
  | `UNPACK
  | `BLAKE2B
  | `SHA256
  | `SHA512
  | `ABS
  | `ADD
  | `AMOUNT
  | `AND
  | `BALANCE
  | `CAR
  | `CDR
  | `CHECK_SIGNATURE
  | `COMPARE
  | `CONCAT
  | `CONS
  | `CREATE_ACCOUNT
  | `CREATE_CONTRACT
  | `IMPLICIT_ACCOUNT
  | `DIP
  | `DROP
  | `DUP
  | `EDIV
  | `EMPTY_MAP
  | `EMPTY_SET
  | `EQ
  | `EXEC
  | `FAILWITH
  | `GE
  | `GET
  | `GT
  | `HASH_KEY
  | `IF
  | `IF_CONS
  | `IF_LEFT
  | `IF_NONE
  | `INT
  | `LAMBDA
  | `LE
  | `LEFT
  | `LOOP
  | `LSL
  | `LSR
  | `LT
  | `MAP
  | `MEM
  | `MUL
  | `NEG
  | `NEQ
  | `NIL
  | `NONE
  | `NOT
  | `NOW
  | `OR
  | `PAIR
  | `PUSH
  | `RIGHT
  | `SIZE
  | `SOME
  | `SOURCE
  | `SENDER
  | `SELF
  | `STEPS_TO_QUOTA
  | `SUB
  | `SWAP
  | `TRANSFER_TOKENS
  | `SET_DELEGATE
  | `UNIT
  | `UPDATE
  | `XOR
  | `ITER
  | `LOOP_LEFT
  | `ADDRESS
  | `CONTRACT
  | `ISNAT
  | `CAST
  | `RENAME
  | `bool
  | `contract
  | `int
  | `key
  | `key_hash
  | `lambda
  | `list
  | `map
  | `big_map
  | `nat
  | `option
  | `or_ [@key "or"]
  | `pair
  | `set
  | `signature
  | `string
  | `bytes
  | `mutez
  | `timestamp
  | `unit
  | `operation
  | `address
  | `SLICE
  (* 005 specific *)
  | `DIG
  | `DUG
  | `EMPTY_BIG_MAP
  | `APPLY
  | `chain_id
  | `CHAIN_ID
  (* 008 specific *)
  | `LEVEL
  | `SELF_ADDRESS
  | `never
  | `NEVER
  | `UNPAIR
  | `VOTING_POWER
  | `TOTAL_VOTING_POWER
  | `KECCAK
  | `SHA3
  | `PAIRING_CHECK
  | `bls12_381_g1
  | `bls12_381_g2
  | `bls12_381_fr
  | `sapling_state
  | `sapling_transaction_deprecated
  | `SAPLING_EMPTY_STATE
  | `SAPLING_VERIFY_UPDATE
  | `ticket
  | `TICKET_DEPRECATED
  | `READ_TICKET
  | `SPLIT_TICKET
  | `JOIN_TICKETS
  | `GET_AND_UPDATE
  | `chest
  | `chest_key
  | `OPEN_CHEST
  | `VIEW
  | `view
  | `constant
  (* 012 specific *)
  | `SUB_MUTEZ
  (* 013 specific *)
  | `tx_rollup_l2_address
  | `MIN_BLOCK_TIME
  | `sapling_transaction
  (* 014 specific *)
  | `EMIT
  (* 015 specific *)
  | `Lambda_rec
  | `LAMBDA_REC
  | `TICKET
  | `BYTES
  | `NAT
] [@@deriving encoding {title="primitive"}, jsoo {enum}]

let primitives : (string * primitive) list = [
  "parameter", `parameter; "storage", `storage; "code", `code; "False", `False;
  "Elt", `Elt; "Left", `Left; "None", `None; "Pair", `Pair; "Right", `Right;
  "Some", `Some; "True", `True; "Unit", `Unit; "PACK", `PACK; "UNPACK", `UNPACK;
  "BLAKE2B", `BLAKE2B; "SHA256", `SHA256; "SHA512", `SHA512; "ABS", `ABS;
  "ADD", `ADD; "AMOUNT", `AMOUNT; "AND", `AND; "BALANCE", `BALANCE; "CAR", `CAR;
  "CDR", `CDR; "CHECK_SIGNATURE", `CHECK_SIGNATURE; "COMPARE", `COMPARE;
  "CONCAT", `CONCAT; "CONS", `CONS; "CREATE_ACCOUNT", `CREATE_ACCOUNT;
  "CREATE_CONTRACT", `CREATE_CONTRACT; "IMPLICIT_ACCOUNT", `IMPLICIT_ACCOUNT;
  "DIP", `DIP; "DROP", `DROP; "DUP", `DUP; "EDIV", `EDIV; "EMPTY_MAP", `EMPTY_MAP;
  "EMPTY_SET", `EMPTY_SET; "EQ", `EQ; "EXEC", `EXEC; "FAILWITH", `FAILWITH;
  "GE", `GE; "GET", `GET; "GT", `GT; "HASH_KEY", `HASH_KEY; "IF", `IF;
  "IF_CONS", `IF_CONS; "IF_LEFT", `IF_LEFT; "IF_NONE", `IF_NONE; "INT", `INT;
  "LAMBDA", `LAMBDA; "LE", `LE; "LEFT", `LEFT; "LOOP", `LOOP; "LSL", `LSL;
  "LSR", `LSR; "LT", `LT; "MAP", `MAP; "MEM", `MEM; "MUL", `MUL; "NEG", `NEG;
  "NEQ", `NEQ; "NIL", `NIL; "NONE", `NONE; "NOT", `NOT; "NOW", `NOW; "OR", `OR;
  "PAIR", `PAIR; "PUSH", `PUSH; "RIGHT", `RIGHT; "SIZE", `SIZE; "SOME", `SOME;
  "SOURCE", `SOURCE; "SENDER", `SENDER; "SELF", `SELF; "STEPS_TO_QUOTA", `STEPS_TO_QUOTA;
  "SUB", `SUB; "SWAP", `SWAP; "TRANSFER_TOKENS", `TRANSFER_TOKENS;
  "SET_DELEGATE", `SET_DELEGATE; "UNIT", `UNIT; "UPDATE", `UPDATE; "XOR", `XOR;
  "ITER", `ITER; "LOOP_LEFT", `LOOP_LEFT; "ADDRESS", `ADDRESS; "CONTRACT", `CONTRACT;
  "ISNAT", `ISNAT; "CAST", `CAST; "RENAME", `RENAME; "bool", `bool; "contract", `contract;
  "int", `int; "key", `key; "key_hash", `key_hash; "lambda", `lambda; "list", `list;
  "map", `map; "big_map", `big_map; "nat", `nat; "option", `option; "or_", `or_;
  "pair", `pair; "set", `set; "signature", `signature; "string", `string;
  "bytes", `bytes; "mutez", `mutez; "timestamp", `timestamp; "unit", `unit;
  "operation", `operation; "address", `address; "SLICE", `SLICE; "DIG", `DIG;
  "DUG", `DUG; "EMPTY_BIG_MAP", `EMPTY_BIG_MAP; "APPLY", `APPLY; "chain_id", `chain_id;
  "CHAIN_ID", `CHAIN_ID; "LEVEL", `LEVEL; "SELF_ADDRESS", `SELF_ADDRESS;
  "never", `never; "NEVER", `NEVER; "UNPAIR", `UNPAIR; "VOTING_POWER", `VOTING_POWER;
  "TOTAL_VOTING_POWER", `TOTAL_VOTING_POWER; "KECCAK", `KECCAK; "SHA3", `SHA3;
  "PAIRING_CHECK", `PAIRING_CHECK; "bls12_381_g1", `bls12_381_g1;
  "bls12_381_g2", `bls12_381_g2; "bls12_381_fr", `bls12_381_fr;
  "sapling_state", `sapling_state; "sapling_transaction_deprecated", `sapling_transaction_deprecated;
  "SAPLING_EMPTY_STATE", `SAPLING_EMPTY_STATE; "SAPLING_VERIFY_UPDATE", `SAPLING_VERIFY_UPDATE;
  "ticket", `ticket; "TICKET_DEPRECATE", `TICKET_DEPRECATED; "READ_TICKET", `READ_TICKET;
  "SPLIT_TICKET", `SPLIT_TICKET; "JOIN_TICKETS", `JOIN_TICKETS;
  "GET_AND_UPDATE", `GET_AND_UPDATE; "chest", `chest; "chest_key", `chest_key;
  "OPEN_CHEST", `OPEN_CHEST; "VIEW", `VIEW; "view", `view; "constant", `constant;
  "SUB_MUTEZ", `SUB_MUTEZ; "tx_rollup_l2_address", `tx_rollup_l2_address;
  "MIN_BLOCK_TIME", `MIN_BLOCK_TIME; "sapling_transaction", `sapling_transaction;
  "EMIT", `EMIT; "Lambda_rec", `Lambda_rec; "LAMBDA_REC", `LAMBDA_REC;
  "TICKET", `TICKET; "BYTES", `BYTES; "NAT", `NAT
]

type primitive_or_macro = [
  | primitive
  | `macro of string
] [@@deriving encoding]

[@@@jsoo
  class type primitive_or_macro_jsoo = js_string
  let primitive_or_macro_to_jsoo = function
    | #primitive as p -> primitive_to_jsoo p
    | `macro s -> string s
  let primitive_or_macro_of_jsoo js =
    try (primitive_of_jsoo js :> primitive_or_macro)
    with _ -> `macro (to_string js)
  let primitive_or_macro_jsoo_conv = primitive_or_macro_to_jsoo, primitive_or_macro_of_jsoo
]

type micheline =
  | Mint of A.zarith [@obj1 "int"]
  | Mstring of string [@obj1 "string"]
  | Mbytes of hex [@obj1 "bytes"]
  | Mprim of { prim: primitive_or_macro;
               args: micheline list [@dft []];
               annots: string list [@dft []] }
  | Mseq of micheline list
[@@deriving encoding {recursive}]

let prim ?(args=[]) ?(annots=[]) prim = Mprim { prim; args; annots }

[@@@jsoo
  class type prim_jsoo = object
    method prim: js_string t optdef readonly_prop
    method args: micheline_jsoo t js_array t optdef readonly_prop
    method annots: js_string t js_array t optdef readonly_prop
  end
  and micheline_jsoo = object
    method int: A.zarith_jsoo t optdef readonly_prop
    method string: js_string t optdef readonly_prop
    method bytes: hex_jsoo t optdef readonly_prop
    inherit prim_jsoo
    inherit [micheline_jsoo t] js_array
  end
  let rec micheline_to_jsoo_aux : ?remove_args:bool -> micheline -> micheline_jsoo t = fun ?(remove_args=true) ->
    function
    | Mint i -> Unsafe.coerce @@ object%js val int = A.zarith_to_jsoo i end
    | Mstring s -> Unsafe.coerce @@ object%js val string = string s end
    | Mbytes h -> Unsafe.coerce @@ object%js val bytes = hex_to_jsoo h end
    | Mprim {prim; args; annots} ->
      let o = object%js
        val prim = primitive_or_macro_to_jsoo prim
        val annots = if annots = [] then undefined else def (of_listf string annots)
        val args = if args = [] && remove_args then undefined else def (of_listf (micheline_to_jsoo_aux ~remove_args) args)
      end in
      remove_undefined o;
      Unsafe.coerce o
    | Mseq a -> Unsafe.coerce @@ of_listf (micheline_to_jsoo_aux ~remove_args) a
  let micheline_to_jsoo = micheline_to_jsoo_aux ~remove_args:true
  let rec micheline_of_jsoo (js : micheline_jsoo t) : micheline =
    match to_optdef A.zarith_of_jsoo js##.int, to_optdef to_string js##.string,
          to_optdef hex_of_jsoo js##.bytes, to_optdef primitive_or_macro_of_jsoo js##.prim ,
          to_bool (Unsafe.global##._Array##isArray js) with
    | Some i, _, _, _, _ -> Mint i
    | _, Some s, _, _, _ -> Mstring s
    | _, _, Some h, _, _ -> Mbytes h
    | _, _, _, Some prim, _ ->
      let args = match to_optdef (to_listf micheline_of_jsoo) js##.args with
        | None -> []
        | Some a -> a in
      let annots = match to_optdef (to_listf to_string) js##.annots with
        | None -> []
        | Some a -> a in
      Mprim {prim; args; annots}
    | _, _, _, _, true -> Mseq (to_listf micheline_of_jsoo (js :> micheline_jsoo t js_array t))
    | _ -> failwith "cannot parse micheline jsoo"
  let micheline_jsoo_conv = micheline_to_jsoo, micheline_of_jsoo
]

type entrypoint =
  | EPdefault
  | EProot
  | EPdo [@jsoo.key "do_"]
  | EPset
  | EPremove
  | EPnamed of string
[@@deriving encoding {title="entrypoint"}, jsoo {enum}]

type script_expr =
  | Micheline of micheline
  | Bytes of hex
[@@deriving encoding {title="script_expr"}]

[@@@jsoo
  type script_expr_jsoo = Ezjs_min.Unsafe.top
  let script_expr_to_jsoo : script_expr -> script_expr_jsoo t = function
    | Micheline m -> Ezjs_min.Unsafe.coerce @@ micheline_to_jsoo m
    | Bytes h -> Ezjs_min.Unsafe.coerce @@ hex_to_jsoo h

  let script_expr_of_jsoo : script_expr_jsoo t -> script_expr = fun js ->
    if Ezjs_min.to_string @@ Ezjs_min.typeof js = "string" then
      Bytes (hex_of_jsoo @@ Ezjs_min.Unsafe.coerce js)
    else Micheline (micheline_of_jsoo @@ Ezjs_min.Unsafe.coerce js)
]

type 'p parameters = {
  entrypoint: entrypoint; [@ddft EPdefault]
  value: 'p;
} [@@deriving encoding, jsoo]

type 'p contract_script = {
  code: 'p;
  storage: 'p;
} [@@deriving encoding {ignore}, jsoo]

(* balance update *)

type freezer_content = {
  fr_delegate: A.pkh;
  fr_cycle: A.uint32;
} [@@deriving encoding {title="freezer_content"}, jsoo]

type freezer =
  | Legacy_rewards of freezer_content [@kind] [@kind_label "category"]
  | Legacy_fees of freezer_content [@kind] [@kind_label "category"]
  | Legacy_deposits of freezer_content [@kind] [@kind_label "category"]
  | Deposits of (A.pkh [@encoding Encoding.(conv (fun s -> (), s) (fun (_, s) -> s) @@ merge_objs unit (obj1 (req "delegate" A.pkh_enc)))]) [@kind] [@kind_label "category"]
[@@deriving encoding {title="freezer"}, jsoo]

type minted =
  | Nonce_revelation_rewards [@kind "nonce revelation rewards"] [@kind_label "category"]
  | Double_signing_evidence_rewards [@kind "double signing evidence rewards"] [@kind_label "category"]
  | Endorsing_rewards [@kind "endorsing rewards"] [@kind_label "category"]
  | Baking_rewards [@kind "baking rewards"] [@kind_label "category"]
  | Baking_bonuses [@kind "baking bonuses"] [@kind_label "category"]
  | Subsidy [@kind] [@kind_label "category"]
  | Invoice [@kind] [@kind_label "category"]
[@@deriving encoding {title="minted"; union}, jsoo]

type burned =
  | Storage_fees [@kind "storage fees"] [@kind_label "category"]
  | Punishments [@kind] [@kind_label "category"]
  | Lost_endorsing_rewards of { delegate: A.pkh; participation: bool; revelation: bool } [@kind "lost endorsing rewards"] [@kind_label "category"]
[@@deriving encoding {title="burned"}, jsoo]

type balance_update_base =
  | Contract of (A.contract [@wrap "contract"]) [@kind]
  | Freezer of freezer [@kind]
  | Accumulator of (unit [@encoding Encoding.(obj1 (req "category" (constant "block fees")))]) [@kind]
  | Minted of minted [@kind]
  | Burned of burned [@kind]
  | Commitment of (string [@encoding Encoding.(conv (fun s -> (), s) (fun (_, s) -> s) @@ obj2 (req "category" (constant "commitment")) (req "committer" string))]) [@kind]
  | Ignore of (string [@encoding Encoding.(conv (fun s -> (), s) (fun (_, s) -> s) @@ merge_objs unit (obj1 (req "kind" string)))])
[@@deriving encoding {title="balance_update_content"}, jsoo]

type balance_update = {
  bu_change: A.zarith;
  bu_update: balance_update_base; [@merge]
  bu_origin: string option;
  bu_delayed_operation_hash: A.operation_hash option;
} [@@deriving encoding {title="balance_updates"}, jsoo]

(* error *)

type node_error = {
  err_kind: string option;
  err_id: string option;
  err_info: (Json_repr.ezjsonm
             [@type Ezjs_min.Unsafe.any]
             [@conv (Js_json.js_of_json, Js_json.json_of_js)])
} [@@deriving jsoo]

let node_error_enc =
  Encoding.conv
    (fun {err_kind; err_id; err_info} ->
       let lid = match err_id with None -> [] | Some id -> [ "id", `String id ] in
       let lkind = match err_kind with None -> [] | Some k -> [ "kind", `String k ] in
       match err_info with
       | `O l -> `O (lkind @ lid @ l)
       | json when lkind = [] && lid = [] -> json
       | _ -> `O (lkind @ lid))
    (fun json ->
       let err_kind, err_id, err_info = match json with
         | `O l ->
           let kind, id, l = List.fold_left (fun (kind, id, l) -> function
               | "kind", `String v -> Some v, id, l
               | "id", `String v -> kind, Some v, l
               | k, v -> kind, id, (k, v) :: l) (None, None, []) l in
           kind, id, `O (List.rev l)
         | _ -> None, None, json in
       {err_kind; err_id; err_info}) @@
  Encoding.any

(* operation *)

type 'p transaction_info = {
  amount: A.uint64;
  destination: A.contract;
  parameters: 'p parameters option;
} [@@deriving encoding {ignore}, jsoo]

type 'p origination_info = {
  balance: A.uint64;
  script: 'p contract_script;
} [@@deriving encoding {ignore}, jsoo]

type tx_rollup_commitment = {
  txrc_level: A.uint32;
  txrc_messages: A.tx_message_result_hash list;
  txrc_predecessor: A.tx_commitment_hash option; [@req]
  txrc_inbox_merkle_root: A.tx_inbox_hash;
} [@@deriving encoding, jsoo]

type sc_rollup_commitment = {
  scrc_compressed_state: A.sc_state_hash;
  scrc_inbox_level: A.uint32;
  scrc_predecessor: A.sc_commitment_hash;
  scrc_number_of_ticks: A.uint64;
} [@@deriving encoding, jsoo]

type transfer_ticket = {
  tt_contents: script_expr; [@json.key "ticket_contents"]
  tt_type: script_expr; [@json.key "ticket_ty"] [@jsoo.key "type_"]
  tt_ticketer: A.contract; [@json.key "ticket_ticketer"]
  tt_amount: A.uint64; [@json.key "ticket_amount"]
  tt_destination: A.contract;
  tt_entrypoint: entrypoint;
} [@@deriving encoding, jsoo]

let unknown_kind_enc =
  let l = ref [] in
  let open Encoding in
  conv
    (fun s -> s, ())
    (fun (s, ()) ->
       if not (List.mem s !l) then (
         Format.eprintf "[tzfunc] Unknown operation: %S@." s;
        l := s :: !l);
       s) @@
  merge_objs (obj1 (req "kind" string)) unit

type dal_slot_header = {
  ds_slot_index: int;
  ds_commitment: string; (* todo: find b58 prefix *)
  ds_commitment_proof: string; (* todo: find b58 prefix *)
} [@@deriving encoding, jsoo]

type 'p manager_operation_kind =
  | Reveal of (A.pk [@wrap "public_key"]) [@kind] [@jsoo.key "reveal"]
  | Transaction of 'p transaction_info [@kind] [@jsoo.key "transaction"]
  | Origination of 'p origination_info [@kind] [@jsoo.key "origination"]
  | Delegation of (A.pkh option [@encoding Encoding.(obj1 (opt "delegate" A.pkh_enc))]) [@kind] [@jsoo.key "delegation"]
  | Event of { type_: micheline; [@enc.key "type"] payload: micheline option; tag: string option } [@kind] [@jsoo.key "event"]
  | Constant of ('p [@wrap "value"]) [@kind "register_global_constant"] [@jsoo.key "constant"]
  | Deposits_limit of (A.uint64 option [@encoding Encoding.(obj1 (opt "limit" A.uint64_enc))]) [@kind "set_deposits_limit"] [@jsoo.key "deposits"]
  | Increase_paid_storage of { amount: A.zarith; destination: A.contract } [@kind]
  | Update_consensus_key of (A.pk [@wrap "pk"]) [@kind]
  | Transfer_ticket of transfer_ticket [@kind]
  | Dal_publish of (dal_slot_header [@wrap "slot_header"]) [@kind "dal_publish_commitment"] [@jsoo.key "dal_publish"]
  | Sr_originate of {pvm_kind: string; kernel: hex; parameters_ty: script_expr; whitelist: A.pkh list [@dft []]} [@kind "smart_rollup_originate"] [@jsoo.key "sr_originate"]
  | Sr_add_messages of (string list [@wrap "message"]) [@kind "smart_rollup_add_messages"] [@jsoo.key "sr_add_messages"]
  | Sr_cement of (A.sc_rollup_hash [@wrap "rollup"]) [@kind "smart_rollup_cement"] [@jsoo.key "sr_cement"]
  | Sr_publish of {rollup: A.sc_rollup_hash; commitment: sc_rollup_commitment} [@kind "smart_rollup_publish"] [@jsoo.key "sr_publish"]
  | Sr_refute of {rollup: A.sc_rollup_hash; opponent: A.pkh; refutation: unit [@encoding Encoding.unit]} [@kind "smart_rollup_refute"] [@jsoo.key "sr_refute"]
  | Sr_timeout of {rollup: A.sc_rollup_hash; stakers: ((A.pkh [@key "alice"]) * (A.pkh [@key "bob"])) [@object] } [@kind "smart_rollup_timeout"] [@jsoo.key "sr_timeout"]
  | Sr_execute of {rollup: A.sc_rollup_hash; cemented_commitment: sc_rollup_commitment; output_proof: string} [@kind "smart_rollup_execute_outbox_message"] [@jsoo.key "sr_execute"]
  | Sr_recover of {rollup: A.sc_rollup_hash; staker: A.pkh} [@kind "smart_rollup_recover_bond"] [@jsoo.key "sr_recover"]
  (* zk_rollup_origination *)
  (* zk_rollup_publish *)
  (* zk_rollup_update *)
  (* host *)
  | Unknown of (string [@encoding unknown_kind_enc]) [@jsoo.key "unknown"]
[@@deriving encoding, jsoo]

type manager_operation_numbers = {
  fee: A.uint64;
  counter: A.zarith;
  gas_limit: A.zarith;
  storage_limit: A.zarith;
} [@@deriving encoding {title="numbers"}, jsoo]

type 'kind manager_operation_info_gen = {
  source: A.contract;
  kind: 'kind; [@merge]
} [@@deriving encoding, jsoo]

type 'p manager_operation_info = 'p manager_operation_kind manager_operation_info_gen
[@@deriving encoding, jsoo]

type big_map_update = {
  bm_key_hash: A.script_expr_hash;
  bm_key: micheline;
  bm_value: micheline option;
} [@@deriving encoding {title="big_map_update"}, jsoo]

type big_map_diff =
  | SDUpdate of (big_map_update list [@wrap "updates"]) [@kind] [@kind_label "action"]
  | SDRemove [@kind] [@kind_label "action"]
  | SDCopy of { source: string; updates: big_map_update list} [@kind] [@kind_label "action"]
  | SDAlloc of {updates: big_map_update list; key_type: micheline; value_type: micheline} [@kind] [@kind_label "action"]
[@@deriving encoding {title="big_map_diff"}, jsoo]

type storage_diff_item =
  | Big_map of { id: A.zarith; diff: big_map_diff } [@kind]
  | Sapling_state of (unit [@encoding Encoding.unit]) [@kind]
 [@@deriving encoding {title="storage_diff"}, jsoo]

type op_status = [`applied | `failed | `skipped | `backtracked]
[@@deriving encoding {title="operation_status"}, jsoo {enum}]

type op_metadata = {
  op_status: op_status;
  op_consumed_milligas: A.zarith; [@dft Z.zero]
  op_balance_updates: balance_update list; [@dft []]
  op_allocated_destination_contract: bool; [@dft false]
  op_originated_contracts: A.contract list; [@dft []]
  op_storage_size: A.zarith; [@dft Z.zero]
  op_storage: micheline option;
  op_paid_storage_size_diff: A.zarith; [@dft Z.zero]
  op_lazy_storage_diff: storage_diff_item list; [@dft []]
  op_errors: node_error list; [@dft []]
  op_originated_rollup: A.tx_rollup_hash option;
  op_global_address: A.script_expr_hash option;
} [@@deriving encoding {option="opt"; ignore; title="operation_metadata"}, jsoo]

type internal_manager_operation = {
  in_content: micheline manager_operation_info; [@merge]
  in_nonce: A.uint32;
  in_result: op_metadata;
} [@@deriving encoding {title="internal_manager_operation"}, jsoo]

type manager_metadata = {
  man_balance_updates: balance_update list; [@dft []]
  man_operation_result: op_metadata;
  man_internal_operation_results: internal_manager_operation list; [@dft []]
} [@@deriving encoding {title="manager_metadata"}, jsoo]

type 'kind manager_operation_gen = {
  man_info: 'kind manager_operation_info_gen; [@merge]
  man_numbers: manager_operation_numbers ; [@merge] [@inherit]
  man_metadata: manager_metadata option
} [@@deriving encoding, jsoo]

type 'p manager_operation = 'p manager_operation_kind manager_operation_gen
[@@deriving encoding, jsoo]

type 'a block_operation = {
  op_protocol: A.protocol_hash;
  op_chain_id: A.chain_id;
  op_hash: A.operation_hash;
  op_branch: A.block_hash;
  op_contents: 'a list;
  op_signature: A.signature option; [@opt]
} [@@deriving encoding, jsoo]

let operations_enc =
  Encoding.(
    conv (fun ops -> (), (), (), ops) (fun (_, _, _, ops) -> ops) @@
    tup4 unit unit unit (list @@ block_operation_enc (manager_operation_enc micheline_enc)))

(* block *)

type fitness = {
  fit_version: A.uint32;
  fit_level: A.uint32;
  fit_pred_round: A.uint32;
  fit_round: A.uint32;
  fit_locked: A.uint32 option;
} [@@deriving jsoo]

let fitness_enc =
  let open Encoding in
  conv
    (fun {fit_version; fit_level; fit_pred_round; fit_round; fit_locked} ->
       let tr = Printf.sprintf "%016lx" in
       [ tr fit_version; tr fit_level; (match fit_locked with None -> "" | Some i -> tr i);
         tr fit_pred_round; tr fit_round ])
    (fun l ->
       let tr s = Int32.of_string ("0x" ^ s) in match l with
       | [ v; lv; lo; p; r] ->
         let tr s = Int32.of_string ("0x" ^ s) in
         { fit_version=tr v; fit_level=tr lv; fit_locked=if lo = "" then None else Some (tr lo);
           fit_pred_round=tr p; fit_round= tr r }
       | [ v; lv ] ->
         { fit_version=tr v; fit_level=tr lv; fit_locked=None;
           fit_pred_round=(-1l); fit_round=(-1l)}
       | _ ->
         { fit_version=(-1l); fit_level=(-1l);
           fit_locked=None; fit_pred_round=(-1l); fit_round= (-1l)}) @@
  list string

type shell = {
  level: A.uint32;
  proto: A.uint32;
  predecessor: A.block_hash;
  timestamp: A.timestamp;
  validation_pass: A.uint32;
  operations_hash: A.operations_hash;
  fitness: fitness;
  context: A.context_hash;
} [@@deriving encoding {title="shell"}, jsoo]

type header = {
  shell : shell; [@merge]
  proof_of_work_nonce: hex;
  seed_nonce_hash: A.nonce_hash option; [@opt]
  signature: A.signature;
} [@@deriving encoding {ignore; title="header"}, jsoo]

type level = {
  nl_level: A.uint32;
  nl_level_position: A.uint32;
  nl_cycle: A.uint32;
  nl_cycle_position: A.uint32;
  nl_expected_commitment: bool
} [@@deriving encoding {ignore; title="level"}, jsoo]

let level_enc = Encoding.(union [
    case (obj1 (req "level" level_enc)) (fun _ -> None) (fun x -> x);
    case (obj1 (req "level_info" level_enc)) (fun x -> Some x) (fun x -> x);
  ])

type metadata = {
  meta_baker: A.pkh;
  meta_level: level; [@merge]
  meta_nonce_hash: A.nonce_hash option; [@dft None]
  meta_consumed_gas: A.uint64; [@dft 0L]
  meta_consumed_milligas: A.uint64; [@dft 0L]
  meta_deactivated: string list;
  meta_balance_updates: balance_update list;
} [@@deriving encoding {ignore; title="block_metadata"}, jsoo]

type 'a block0 = {
  protocol: A.protocol_hash;
  chain_id: A.chain_id;
  hash: A.block_hash;
  header: header;
  metadata: metadata option;
  operations: 'a
} [@@deriving encoding {ignore}, jsoo]

type block = A.operation_hash list block0
[@@deriving encoding {title="block_short"}, jsoo]

type full_block = (micheline manager_operation block_operation list [@encoding operations_enc]) block0
[@@deriving encoding {title="block"}, jsoo]

type account = {
  ac_balance : A.uint64;
  ac_delegate : A.pkh option;
  ac_script : micheline contract_script option;
  ac_counter : A.zarith option;
} [@@deriving encoding {option="opt"; title="proto_account"}, jsoo]

let time_between_blocks_enc = Encoding.(
    union [
      case (obj1 (req "minimal_block_delay" string))
        (fun f -> Some (string_of_float f)) float_of_string;
      case (obj1 (req "time_between_blocks" (array string)))
        (fun _ -> None) (fun a -> float_of_string a.(0));
    ]
  )

type constants = {
  hard_gas_limit_per_operation : A.zarith;
  hard_gas_limit_per_block : A.zarith;
  hard_storage_limit_per_operation : A.zarith;
  time_between_blocks : float; [@encoding time_between_blocks_enc] [@merge]
} [@@deriving encoding {ignore; title="constants"}, jsoo]
